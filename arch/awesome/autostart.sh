#!/usr/bin/env bash

# wal -R
autorandr -c
nm-applet &

setxkbmap \
  -layout "us(colemak_dh)" \
  -option "" \
  -option caps:hyper \
  -option shift:both_capslock

~/.config/keys/tap-mods.sh # xcape
~/.fehbg & # wallpaper

pgrep -U $UID "clipmenud" || clipmenud &
pgrep -U $UID "dunst" || dunst --startup_notification true &
# pgrep -U $UID "picom"  || picom --config ~/.config/picom/picom.conf &

# sleep 0.5
# pgrep -U $UID "conky" || ~/.config/awesome/conky_start

sleep 0.8
~/.config/polybar/launch.sh &

