local awful = require("awful")
local gears = require("gears")

local mods = require('.bindings.mods')
local super = mods.super
local shift = mods.shift
local ctrl = mods.ctrl

local function fullscreen(c)
  c.fullscreen = not c.fullscreen
  c:raise()
end

client.connect_signal('request::default_keybindings', function()
  awful.keyboard.append_client_keybindings({
    awful.key(
      { super }, "f",
      fullscreen,
      { description = "toggle fullscreen", group = "client" }
    ),
    awful.key(
      { super }, "x",
      fullscreen,
      { description = "toggle fullscreen", group = "client" }
    ),

    awful.key(
      { super, shift}, 'q',
      function(c)
        c:kill()
      end,
      { description = 'close', group = 'client' }
    ),

    awful.key(
      { super }, "o",
      function(c)
        c:move_to_screen()
      end,
      { description = "move to screen", group = "client" }
    )
  })

  for i = 1, 9 do
    -- Move client to tag.
    awful.keyboard.append_client_keybinding(
      awful.key(
        { super, shift }, "#" .. i + 9,
        function(c)
          local tag = c.screen.tags[i]

          if tag then
            c:move_to_tag(tag)
          end
        end,
        { description = "move focused client to tag #" .. i, group = "tag" }
      )
    )
  end
end)
