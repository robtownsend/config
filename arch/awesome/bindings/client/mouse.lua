local awful = require('awful')

client.connect_signal('request::default_mousebindings', function()
   awful.mouse.append_client_mousebindings{
      awful.button{
         modifiers = {},
         button    = 1,
         on_press  = function(c) c:activate{context = 'mouse_click'} end
      }
  }
end)

