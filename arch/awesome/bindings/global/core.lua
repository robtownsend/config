local awful = require("awful")

local mods = require('.bindings.mods')
local super = mods.super
local hyper = mods.hyper
local shift = mods.shift
local ctrl = mods.ctrl
local print = mods.print

local function swap(dir)
  return function()
    awful
      .client
      .swap
      .global_bydirection(
        dir,
        client.focus
      )
  end
end

awful.keyboard.append_global_keybindings {
  -- Standard WM controls
  awful.key(
    { super, shift }, "r",
    awesome.restart,
    { description = "reload awesome", group = "awesome" }
  ),
  awful.key(
    { hyper, shift }, "q",
    awesome.quit,
    { description = "quit awesome", group = "awesome" }
  ),

  -- Launcher

  awful.key(
    { super }, 'space',
    function()
      awful.spawn('rofi -modi run,drun -show drun -show-icons -theme slate')
    end,
    { description = 'launcher', group = 'awesome' }
  ),
  awful.key(
    { super }, 'd',
    function()
      awful.spawn('rofi -modi run,drun -show drun -show-icons -theme slate')
    end,
    { description = 'launcher', group = 'awesome' }
  ),
  awful.key(
    { hyper }, 'space',
    function()
      awful.spawn('rofi -modi run,drun -show drun -show-icons -theme slate')
    end,
    { description = 'launcher', group = 'awesome' }
  ),
  awful.key(
    { super }, "Return",
    function()
      awful.spawn('kitty --directory ~')
    end
  ),

  -- Lock Screen
  awful.key(
    { hyper, shift }, 'l',
    function()
      awful.spawn('betterlockscreen -l dimblur')
    end,
    { description = 'lock screen', group = 'awesome' }
  ),

  -- Screen brightness

  awful.key(
    {}, 'XF86MonBrightnessUp',
    function()
      awful.spawn('light -A 5')
    end,
    { description = 'increase screen brightness', group = 'awesome' }
  ),
  awful.key(
    {}, 'XF86MonBrightnessDown',
    function()
      awful.spawn('light -U 5')
    end,
    { description = 'increase screen brightness', group = 'awesome' }
  ),
  awful.key(
    { super, shift }, 'Left',
    swap('left'),
    { description = 'swap with next client', group = 'client' }
  ),
  awful.key(
    { super, shift }, 'Right',
    swap('right'),
    { description = 'swap with next client', group = 'client' }
  ),
  awful.key(
    { super, shift }, 'Up',
    swap('up'),
    { description = 'swap with next client', group = 'client' }
  ),
  awful.key(
    { super, shift }, 'Down',
    swap('down'),
    { description = 'swap with next client', group = 'client' }
  )
  -- awful.key(
  --   { print }, "y",
  --   function()
  --     awful.spawn("notify-send 'print-y pressed'")
  --     awful.spawn('nvim')
  --   end,
  --   { description = "test awesome", group = "awesome" }
  -- )
}

for i = 1, 9 do
  awful.keyboard.append_global_keybinding(
    -- View tag only.
    awful.key(
      { super }, "#" .. i + 9,
      function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
          tag:view_only()
        end
      end,
      { description = "view tag #" .. i, group = "tag" }
    )
  )
end
