local awful = require("awful")
local hyper = require('.bindings.mods').hyper

awful.screen.connect_for_each_screen(function(s)
  s.workspace_notification_id = nil
end)

local function move(grid, direction)
  return function ()
    local start = awful.screen.focused().selected_tag
    grid:navigate(direction)
    local current = awful.screen.focused().selected_tag

    if (start.index ~= current.index) then
      grid:on_tag_selected(current)
    end
  end
end

return function(grid)
  awful.keyboard.append_global_keybindings {
    awful.key(
      { hyper }, "Up",
      move(grid, "up"),
      {description = "Up", group="Tag"}
    ),
    awful.key(
      { hyper },
      "Down",
      move(grid, "down"),
      {description = "Down", group="Tag"}
    ),
    awful.key(
      { hyper },
      "Left",
      move(grid, "left"),
      {description = "Left", group="Tag"}
    ),
    awful.key(
      { hyper },
      "Right",
      move(grid, "right"),
      {description = "Right", group="Tag"}
    )
  }
end

