return function(opts)
  local grid = opts.grid
  local scratchpads = opts.scratchpads

  require('.bindings.global.core')
  require('.bindings.global.scratchpads')(scratchpads)
  require('.bindings.global.grid')(grid)
end

