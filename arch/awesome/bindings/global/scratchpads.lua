local awful = require("awful")
local hyper = require('.bindings.mods').hyper

return function(scratchpads)
  local scratchterm = scratchpads.scratch
  local pulse = scratchpads.pulse
  local clipmenu = scratchpads.clipmenu
  local lazydocker = scratchpads.lazydocker

  awful.keyboard.append_global_keybindings {
    awful.key(
      { hyper }, "q",
      function ()
        scratchterm:toggle()
      end
    ),

    awful.key(
      { hyper }, "a",
      function ()
        pulse:toggle()
      end
    ),

    awful.key(
      { hyper }, "c",
      function ()
        clipmenu:toggle()
      end
    ),

    awful.key(
      { hyper }, "d",
      function ()
        lazydocker:toggle()
      end
    )
  }
end
