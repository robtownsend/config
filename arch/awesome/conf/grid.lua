local workspace_grid = require("awesome-workspace-grid")

return workspace_grid({
  rows = 3,
  columns = 3,
  icon_size = 50,
  visual = false,
  switch_all_screens = false,
})
