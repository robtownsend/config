local super = require('.bindings.mods').super

require("awesomewm-vim-tmux-navigator")({
	mod = super,
	mod_keysym = "Super_L", -- comment out to autodetect
  -- Navigate with arrows
	up = { "Up", "Up" },
	down = { "Down", "Down" },
	left = { "Left", "Left" },
	right = { "Right", "Right" },

	tmux = {
		mods = { "Control_L" },
		up = "Up",
		down = "Down",
		left = "Left",
		right = "Right",
	},

	vim = {
		mods = { "Control_L" },
		up = "k",
		down = "j",
		left = "h",
		right = "l",
	},

	-- focus = require("awful").client.focus.global_bydirection,

	-- dont_restore_mods = true, -- prevent sticky mods (see troubleshooting)
	use_pstree = true, -- detect app by using pstree instead of dynamic titles
	-- use_xdotool = true, -- emulate keypresses using xdotool instead of builtin
})
