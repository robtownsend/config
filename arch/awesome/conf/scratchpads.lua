local awful = require("awful")
local bling = require('bling')
-- local ruled = awful.rules
local ruled = require('ruled')
local center = require('utils.center')

local debug = require('utils.debug')

client.connect_signal('request::activate', function(c)
  if (not c.class) then
    return
  end

  local class = c.class:lower()

  if (
    class == "scratchterm" or
    class == "pavucontrol" or
    class == "clipmenu" or
    class == "lazydocker-scratchpad"
  ) then
    center(c)
  end

end)

local scratchterm = bling.module.scratchpad({
  -- command = 'kitty --class scratchterm tmux -s scratch',
  command = 'kitty --class scratchterm',
  rule = { instance = 'scratchterm' },
  autoclose = false,
  floating = true,
  sticky = false,
  dont_focus_before_close = true,
  geometry = {x=0, y=0, height=30, width=120},
})

local pulseaudio_scratchpad = bling.module.scratchpad({
  command = 'pavucontrol',
  rule = { instance = 'pavucontrol' },
  autoclose = false,
  floating = true,
  sticky = false,
  dont_focus_before_close = true,
  geometry = {x=0, y=0, height=0, width=0},
})

local clipmenu_scratch = bling.module.scratchpad({
  command = 'kitty --class clipmenu clipmenu',
  rule = { instance = "clipmenu" },
  autoclose = false,
  floating = true,
  dont_focus_before_close = true,
  sticky = false,
  geometry = {x=0, y=0, height=0, width=0},
})

local lazydocker_scratch = bling.module.scratchpad({
  command = 'kitty --class lazydocker-scratchpad lazydocker',
  rule = { instance = "lazydocker-scratchpad" },
  autoclose = false,
  floating = true,
  dont_focus_before_close = true,
  sticky = false,
  geometry = {x=0, y=0, height=0, width=0},
})

return {
  scratch = scratchterm,
  pulse = pulseaudio_scratchpad,
  clipmenu = clipmenu_scratch,
  lazydocker = lazydocker_scratch,
}
