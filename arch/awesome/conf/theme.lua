-- Theme handling
local beautiful = require("beautiful")
local gears = require("gears")
local awful = require("awful")

beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
beautiful.wallpaper = "/home/rob/.config/awesome/wallhaven-76ey3v_1920x1080.png"

awful.screen.set_auto_dpi_enabled(true)
local global_wallpaper = awful.wallpaper {
    widget = {
        {
            image  = beautiful.wallpaper,
            resize = true,
            widget = wibox.widget.imagebox,
        },
        valign = "center",
        halign = "center",
        tiled  = false,
        widget = wibox.container.tile,
    }
}

screen.connect_signal("request::wallpaper", function()
    -- screen is the global screen module. It is also a list of all screens.
    global_wallpaper.screens = screen
end)
