local awful = require("awful")
local ruled = require("ruled")

-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")
require("awful.autofocus")

tag.connect_signal("request::default_layouts", function()
  awful.layout.append_default_layout(
    awful.layout.suit.tile
  )
end)

ruled.client.connect_signal('request::rules', function()
   ruled.client.append_rule{
      id         = 'global',
      rule       = {},
      properties = {
         focus     = awful.client.focus.filter,
         raise     = true,
         screen    = awful.screen.preferred,
         placement = awful.placement.no_overlap + awful.placement.no_offscreen
      }
   }
end)

-- Core
pcall(require, ".conf.handle_error_signals")
pcall(require, ".conf.theme")
pcall(require, ".conf.bootstrap_screens")
pcall(require, ".conf.nav")
pcall(require, ".conf.focus_follows_mouse")

require('bindings')({
  scratchpads = require("conf.scratchpads"),
  grid = require('conf.grid')
})

awful.spawn.with_shell("~/.config/awesome/autostart.sh")

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
  -- Set the windows at the slave,
  -- i.e. put it at the end of others instead of setting it master.
  -- if not awesome.startup then awful.client.setslave(c) end

  if (
    awesome.startup
    and not c.size_hints.user_position
    and not c.size_hints.program_position
  ) then
    -- Prevent clients from being unreachable after screen count changes.
    awful.placement.no_offscreen(c)
  end
end)

