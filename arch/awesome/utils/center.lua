local awful = require('awful')

local center = function(c)
  c.width = awful.screen.focused().workarea.width * 0.7
  c.height = awful.screen.focused().workarea.height * 0.8

  awful.placement.centered(c)
end

return center
