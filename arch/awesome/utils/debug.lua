local naughty = require('naughty')

local function debug(str)
  naughty.notify({
    text = str
  })
end

return debug
