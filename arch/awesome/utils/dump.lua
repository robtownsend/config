local naughty = require('naughty')

local function tableToString(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. tableToString(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

local function dump(t)
  local str = tableToString(t)

  naughty.notify({
    text = str
  })
end

return dump
