#!/usr/bin/env bash
set -e

CONKY_DIR="$HOME/.config/conky"
cpu_dir="$HOME/.config/conky/cpu"

for file in $cpu_dir/*.conf; do
  conky -c "$file"
done

conky -c "$CONKY_DIR/conky-taskwarrior.conf"
conky -c "$CONKY_DIR/conky-tmux.conf"
