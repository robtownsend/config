#!/bin/bash

wal -R
autorandr -c
nm-applet &

setxkbmap -layout "us(colemak_dh)" -option "" -option caps:hyper -option shift:both_capslock
~/.config/keys/tap-mods.sh
~/.fehbg &

pgrep -U $UID "clipmenud" || clipmenud &
pgrep -U $UID "sxhkd" || sxhkd &
pgrep -U $UID "dunst" || dunst --startup_notification true &
pgrep -U $UID "picom"  || picom --experimental-backends --config ~/.config/picom/picom.conf &

sleep 0.5
conky_dir="$HOME/.config/conky"
pgrep -U $UID "conky" || "$conky_dir/conky-start.sh" &

sleep 0.8
~/.config/polybar/launch.sh &

