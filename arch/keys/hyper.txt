Remapping caps lock to hyper took an unexpectedly long time. Some notes for if I need to do this again down the road:

xmodmap is pretty fasy to get running, and it's what many of the dotfiles I saw used.Ths is a very brittle solution though. If the system is docked or goes to sleep, these bindings are cleared, and weren't being automatically restored. From what I could gather, this has something to do with how X builds its keymap/layout from scratch at each of these events (plus more ofc)

So instead, I've moved over to xkb, a much more robust solution, although more complex to configure. 

Long story short, the setxkbmap command with the caps:hyper option gets you about halffway there. The trouble then is that the hyper key is mapped to mod4 by default, same as super. To differentiate them, you need to reassign it to mod3. This was tricky since documentation on this is fucking awful, butultimately only two edits are needed:

First the virtual hyper key must be deleted. Comment out line 581 in /usr/share/X11/xkb/keycodes/evdev
     // <HYPR> =   207;

Next, in /usr/share/X11/xkb/symbols/capslock:66-70

hidden partial modifier_keys
xkb_symbols "hyper" {
  key <CAPS> { [ Hyper_L ] };
  modifier_map Mod3 { <CAPS> };
};

And you should be good to go
