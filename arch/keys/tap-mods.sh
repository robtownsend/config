#!/bin/sh

# Defines behavior of modifiers when tapped (i.e. pressed and released)

pkill xcape

# Left Control -> Vim window prefix
xcape -e "Control_L=Control_L|Alt_L|w"

# CapsLock -> tmux prefix
xcape -e "#66=Control_L|a"

# Super (ie win key) -> wmfocus
# xcape -e "Super_L=Hyper_L|w"

# Left Shift -> :
xcape -e "Shift_L=Shift_L|semicolon"

# Right Shift -> "
xcape -e "Shift_R=Shift_R|apostrophe"

# Right Control -> FloatermToggle nvim
xcape -e "Control_R=Alt_L|Shift_L|t"

xcape -e "Alt_L=Control_L|Shift_L|Alt_L|h"
