#!/usr/bin/env zsh

utils_dir="/home/rob/dev/conf/dots/lib/utils"
source "$utils_dir/taskwarrior_active_tasks.sh"
source "$utils_dir/get_json_array_length.sh"
source "$utils_dir/get_json_prop.sh"
source "$utils_dir/truncate_str.sh"

active_tasks=$(taskwarrior_active_tasks)
num_active_tasks=$(get_json_array_length "$active_tasks")

if [[ "$num_active_tasks" -eq 0 ]]; then
  echo ""
  exit 0
fi

tasks=$(echo "$active_tasks" | jq '.[0]')
task_id=$(get_json_prop "$tasks" 'id')
task_desc=$(get_json_prop "$tasks" 'description')

echo "$task_id: $(truncate_str "$task_desc")"
