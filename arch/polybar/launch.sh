#!/usr/bin/env bash

config=$HOME/.config/polybar/config.ini

pkill polybar

polybar --config=$config main-monitor 2>&1 | tee -a /tmp/main-monitor.log & disown
polybar --config=$config magi 2>&1 | tee -a /tmp/magi.log & disown
polybar --config=$config gazebo-monitor 2>&1 | tee -a /tmp/gazebo-monitor.log & disown
polybar --config=$config laptop 2>&1 | tee -a /tmp/laptop-screen.log & disown
