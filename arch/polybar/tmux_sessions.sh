#!/usr/bin/env bash

sessions=$(tmux ls 2> /dev/null)
if [[ "$sessions" == "" ]]; then
  echo ""
  exit 0
fi

readarray -t lines < <(echo "$sessions")
for line in "${lines[@]}"; do
  session=$(echo "$line" | awk '{split($0, arr, ":"); print arr[1]}')
  status=""

  if echo "$line" | grep -q 'attached'; then
    status="" # attached
  else
    status="" # detached
  fi

  echo "${status} ${session} "
done
