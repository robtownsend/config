setopt inc_append_history

export ZDOTDIR=$HOME/.config/zsh
export ZGEN_AUTOLOAD_COMPINIT=false
export DOTS_DIR=$HOME/dev/conf/dots
export UTILS_DIR=$DOTS_DIR/lib/utils
export WRITINGS_DIR=$HOME/data/writings
export SHELL=/usr/bin/zsh
export APPIMAGES_DIR=$HOME/.appimages
export VISUAL=nvim
export EDITOR=nvim
export ZSH_DOTENV_PROMPT=false

export TW_DIR=$HOME/.config/taskwarrior
export TIMEW_DIR=$HOME/.timewarrior
export TASKRC=$TW_DIR/taskrc
export GOPATH="$HOME/.go"
export KAFKA_BIN_DIR="/opt/kafka/bin"

export PATH=$DOTS_DIR/bin:$PATH
export PATH=$PATH:/sbin
export PATH=$PATH:~/.bin
export PATH=$PATH:~/.local/.bin
export PATH=$PATH:$APPIMAGES_DIR
export PATH=$PATH:~/.nodenv/bin
export PATH=$PATH:~/.cargo/bin
export PATH=$PATH:~/.local/py_venv/bin
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:$TW_DIR/cmds
export PATH=$PATH:$TW_DIR/cmds/subtasks
export PATH=$PATH:$TW_DIR/cmds/subtasks
export PATH=$PATH:$TIMEW_DIR/cmds
export PATH=$PATH:$KAFKA_BIN_DIR

export PAGER="batcat"

export MAGIC_ENTER_GIT_COMMAND=gs
export MAGIC_ENTER_OTHER_COMMAND=ls

export CM_LAUNCHER=fzf
export CM_HISTLENGTH=50000

export BAT_THEME="Coldark-Dark"

export FZF_DEFAULT_COMMAND="rg --files --follow --no-ignore-vcs --hidden -g '!{**/node_modules/*,**/.git/*}'"
export FZF_DEFAULT_OPTS="
  --height 100%
  --multi
  --preview 'batcat --color=always --style=full --line-range=:500 {}'
  --preview-window 'bottom,hidden'
  --bind 'alt-a:toggle-preview,alt-y:execute-silent(echo {+} | copymenu),alt-x:execute(rm -i {+})+abort,alt-l:clear-query'
"

export WORKTREE_DIR="$HOME/dev/.worktrees"

