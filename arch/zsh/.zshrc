# zmodload zsh/zprof

autoload -U menu-search
zle -N menu-search
autoload -U recent-paths
zle -N recent-paths

source_conf() {
  source "$ZDOTDIR/plugins.zsh"
  local dirs=(aliases utils conf dev bindings sys_conf)

  for d in "${dirs[@]}"; do
    for src in "$ZDOTDIR/$d"/**/*.zsh; do
      # shellcheck source=/dev/null
      source "$src"
    done
  done
}

source_conf
load_shell_funcs

if [ -e "$HOME/.cache/wal/sequences" ]; then
  (cat ~/.cache/wal/sequences &)
fi

if [[ -f "$ZDOTDIR/.secrets" ]]; then
  source "$ZDOTDIR/.secrets"
fi
# zprof
