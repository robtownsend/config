### General ###
alias to='touch'
alias tox='touch_exec'
alias rm='rm -I'
alias md='mkdir -p'
alias cpr='cp -R'
alias ls='eza --icons --group-directories-first'
alias cd='z'
alias own='chown $USER:$USER'
alias se='sudoedit'
alias x='chmod +x'
alias sudo='sudo ' # trailing space allows running user-specific aliases from sudo (somehow)
alias rename='file-rename --nopath --verbose' # edit only filenames; dont touch parent dirs
alias w='watch'

alias g='rg --smart-case'

### Pager ###
alias b='bat'
alias bat='batcat' # debian-specific
alias man='batman'
# choose a theme that highlights flags
alias batman='BAT_THEME="Monokai Extended" batman'


### Disk / Mem ###
alias free='free -h'
alias df='duf'
alias du='dua'
alias duh='du'

### Custom Helpers ###
alias h='help'
alias hist='history | tac | $PAGER'
alias p='page'
alias vr='version'

### Package Manager ###
alias s='search_pkgs'
alias i='sudo apt install --yes'
alias u='update_system'
alias rp='sudo apt remove'
alias hold='sudo apt-mark hold'

### zsh ###
alias zgr='zgenom reset'

### Kernel ###
# -u Update existing initramfs
# -k all Update all installed kernels with existing initramfs
alias ui='sudo update-initramfs -uk all'
alias dm='sudo dmesg --human --color' # --human pages output
