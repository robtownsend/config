### Editor Shortcuts ###
alias v='nvim --startuptime /tmp/nvim-startuptime'
alias vim='nvim --startuptime /tmp/nvim-startuptime'

### Shell shortcuts ###
alias sz='source $ZDOTDIR/.zshrc'
alias lg='lazygit'

alias dc='docker-compose'
alias dl='docker logs'

alias clear-docker-containers='docker rm -vf $(docker ps -aq)'
alias clear-docker-images='docker rmi -f $(docker images -aq)'

alias kc='kubectl'
alias ke='kubectl exec -it'
alias de='docker exec -i -t'
alias dr='docker run -i -t' # -it breaks zsh autocomplete for some reason
alias dc='docker compose'
alias dps='docker ps'

alias tf='terraform'

alias ni='npm install'
alias nis='npm install -S'
alias nu='npm uninstall'
alias nus='npm uninstall -S'
alias nr='npm run'
