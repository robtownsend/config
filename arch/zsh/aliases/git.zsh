# Branching
alias gc='git checkout'
alias gcb='git checkout -b'
alias gb='git branch'

# Status
alias gs='git status -s'
alias gsl='git status'

alias gcm='git commit'
alias ga='git add -p'
alias gan='git add'
alias gpl='git pull'
alias gps='git push'
alias gpu='git_push_upstream'

# Diff
alias gd='git diff'
alias gdc='git diff --cached'


alias mt='git mergetool'
alias dt='git difftool'

# Logging
alias gg='git graph'
alias gl='git log-relative'
alias glr='git log-relative'
alias gla='git log-abs'
alias gls='git log-stats'

# Worktrees
alias gwa='worktree_add'
alias gwp='git worktree prune'
alias gwl='git worktree list'

alias gfh='git file-history'

# Utils
alias gv='git view'

