alias lzd='lazydocker'

### Pass ###
alias pc='pass -c'
alias pc2='pass -c2'
alias pc3='pass -c3'
alias pe='pass edit'
alias pim='pass insert -m'
alias pg='pass git'
alias pgen='pass generate -c'
alias pinsm='pass insert -m'

alias pwgen='pwgen -N 1'

alias screenshot="scrot --silent --quality 100 --select --ignorekeyboard"
alias mi='mirage .'
alias ch='cheat'
alias av='auto_version'
alias a='agenda'
alias nd='notify_device'
alias m='member'

### Taskwarrior ###
alias t='task'
alias ts='task sync'
alias tm='task_mod'
alias td='task_done'
alias te='task_edit'
alias tst='task_start_toggle'
alias tp='page task'
alias pt='tp'
alias st='subtasks'

alias tw='timew'
alias tn='tasknote'
alias tnn='tasknote new'


### Writing keybinds ###
alias j='journal'
alias jy='journal yesterday'
alias n='nvim -c "Neorg workspace notes"'
alias wr='nvim -c "Neorg workspace experiments"'

alias mx='tmux'
alias mbr='remember'

### Utils ###
alias vc='veracrypt -t'
# alias nnn='nnn -cdDreEoP p'
# alias n='nnn -o' # Always pick, never open
alias dw='diceware --no-caps -d -'
alias iv='sudo ventoy -i -r 4096'
alias ivf='sudo ventoy -I -r 51200' # 50 GB

### Display ###

alias bl='light -S'
alias rs='redshift -P -O'
alias rsx='redshift -x'
alias rs1='rs 1000'
alias rs2='rs 2000'
alias rs3='rs 3000'
alias rs4='rs 4000'
alias rs5='rs 5000'
alias rs6='rs 6000'
alias rsm='rs1' # Stay in home row

### VPN ###
alias vpn='protonvpn-cli c'
alias vpnf='vpn -f'
alias vpnp='vpn --p2p'
alias vpnd='protonvpn-cli d'

