# Ctrl-e opens current line in editor

autoload -U edit-command-line
zle -N edit-command-line
bindkey '^e' edit-command-line
