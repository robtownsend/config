# magic_space derived from ohmyzsh/sudo sudo-command-line function definition

# In zle widgets, the contents of the editing buffer is made available in the
# $BUFFER variable. $LBUFFER contains what's left of the cursor (same as
# $BUFFER[1,CURSOR-1]) and $RBUFFER what's to the right (same as
# $BUFFER[CURSOR,-1]).
# https://unix.stackexchange.com/questions/152263/how-can-you-get-the-current-terminal-line-the-one-that-is-still-editable-by-the
__magic_space() {
  # If line is empty, get the last run command from history
  local cmd
  if [[ -z $BUFFER ]]; then
    cmd="$(fc -ln -1)"
  else
    cmd=$BUFFER
  fi

  setopt rematchpcre
  if [[ $cmd =~ '^(h|help)[\s]+.*$' ]]; then
    # toggle h if cmd was either "h $cmd" or "help $cmd"
    rest=$(echo "$cmd" | sed -E 's/^(help|h)[[:space:]]+//')
    BUFFER="$rest"
  elif [[ $cmd =~ '^man[\s]+.*' ]]; then
    # Replace 'man <cmd>' or 'man <digit> <cmd>' with 'h <cmd>'
    rest=$(echo "$cmd" | sed -E 's/^man[[:space:]]+[[:digit:]]?[[:space:]]+/h /')
    BUFFER="h $rest"
  else
    BUFFER="h $cmd"
  fi
  CURSOR="${#BUFFER}" # set cursor to eol
  unsetopt rematchpcre

  zle && zle redisplay # show updated line contents
}

autoload -U __magic_space
zle -N __magic_space
# Binding to '  ' (two spaces) caused delays in regular typing where the shell
# would pause after a space to wait for the binding. So instead a code mapped
# by xcape to left alt is used
# keycode sent by # ctrl-shift-alt-h. Use showkeys -a to check
bindkey '^[[104;8u' __magic_space
