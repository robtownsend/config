load_br() {
  local br_path="$HOME/.config/broot/launcher/bash/br"

  if [[ -e "$br_path" ]]; then
    source "$br_path"
  fi
}

load_br
