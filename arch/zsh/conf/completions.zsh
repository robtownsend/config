# Compinit handled by zgenom

load_completions() {
  local cmd
  local dest
  local completion_dir=$HOME/.local/share/zsh/completions

  for f in "$ZDOTDIR"/completions/**/*; do
    dest="$completion_dir/$(basename $f)"
    cmd=$(basename "$f" | sed 's/_//')

    if [[ ! -e $dest ]]; then
      cp "$(realpath $f)" "$dest"
    fi

    load_completion_file "$cmd" "$dest"
  done
}

# zstyle ':completion:*' completer _expand _complete
zstyle ':completion:*' menu select
zstyle ':completion:*' group-name ''
zstyle ':completion:*:descriptions' format '%d'
# zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*' insert-sections true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

complete -cf mount
complete -cf sudo
compdef '_dispatch man man' batman
compdef '_dispatch man man' help

declare -A autocompletions=(
  [kubectl]="kubectl completion zsh"
  [kind]="kind completion zsh"
  [minikube]="minikube completion zsh"
  [helm]="helm completion zsh"
)

for cmd generate_cmd in ${(@kv)autocompletions}; do
  load_cmd_completion $cmd $generate_cmd
done

load_completions
