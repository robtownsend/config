export ZPWR_EXPAND=false

# aliases expand in second position after sudo
export ZPWR_EXPAND_SECOND_POSITION=false

# Dont expand globs, history etc
# Allows cycling through expand options
export ZPWR_EXPAND_NATIVE=true

# Dont expand aliases after spelling correction
export ZPWR_CORRECT_EXPAND=false

# expand into history any unexpanded
export ZPWR_EXPAND_TO_HISTORY=false

