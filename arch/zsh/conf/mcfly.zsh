if cmd_exists "mcfly"; then
  eval "$(mcfly init zsh)"
fi

export MCFLY_RESULTS=50
export MCFLY_FUZZY=4
export MCFLY_INTERFACE_VIEW=BOTTOM
