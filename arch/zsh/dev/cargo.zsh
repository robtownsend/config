install_cargo_deps() {
  declare -A cargo_deps=(
    ["broot"]="broot"
    ["cargo-update"]="cargo-update"
    ["cobalt"]="cobalt-bin"
    ["dua"]="dua-cli"
    ["eza"]="eza"
    ["hurl"]="hurl"
    ["mcfly"]="mcfly"
    ["procs"]="procs"
    ["replacer"]="replacer-cli"
    ["sd"]="sd"
    ["starship"]="starship --locked"
    ["stylua"]="stylua"
    ["tree-sitter"]="tree-sitter-cli"
  )

  cargo_pkg_is_installed() {
    local pkg="$1"

    if [[ $pkg == 'cargo-update' ]]; then
      return $(blackhole "cargo install-update --version")
    fi

    return $(cmd_exists "$pkg")
  }

  for k v in "${(@kv)cargo_deps}"; do
    if cargo_pkg_is_installed "$k"; then
      continue
    fi

    eval "cargo install $v"
  done
}

install_cargo_deps
