install_go_pkgs() {
  declare -A deps=(
    ["go-global-update"]="github.com/Gelio/go-global-update@latest"
    ["kind"]="sigs.k8s.io/kind@latest"
    ["lazydocker"]="github.com/jesseduffield/lazydocker@latest"
    ["kepubify"]="github.com/pgaskin/kepubify@latest"
    ["dlv"]="github.com/go-delve/delve/cmd/dlv@latest"
  )

  for k v in ${(@kv)deps}; do
    if cmd_exists "$k"; then
      continue
    fi

    go install "$v"
  done
}

install_go_pkgs
