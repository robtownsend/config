install_npm_pkgs() {
  declare -A deps=(
    ["bash-language-server"]="bash-language-server"
    ["awk-language-server"]="awk-language-server"
  )

  for k v in ${(kv)deps}; do
    if cmd_exists "$k"; then
      continue
    fi

    echo "$k not found; installing with npm"
    sudo npm install --global "$v"
  done
}

install_npm_pkgs
