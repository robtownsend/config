# load zgenom
if [[ ! -f "$HOME/.zgenom/zgenom.zsh" ]]; then
  git clone https://github.com/jandamm/zgenom.git "${HOME}/.zgenom"
fi
source ~/.zgenom/zgenom.zsh

# Needed for zsh-syntax-highlighting
zle -N menu-search
zle -N recent-paths

zgenom autoupdate
if ! zgenom saved; then
    echo "Creating a zgenom save"

    # zgenom load johnhamelink/env-zsh
    zgenom load marlonrichert/zsh-autocomplete

    zgenom ohmyzsh
    zgenom ohmyzsh plugins/magic-enter
    zgenom ohmyzsh plugins/zoxide
    zgenom ohmyzsh plugins/command-not-found
    # zgenom ohmyzsh plugins/dotenv
    zgenom ohmyzsh plugins/sudo
    zgenom ohmyzsh plugins/git-extras
    zgenom ohmyzsh plugins/gatsby
    zgenom ohmyzsh --completion plugins/terraform
    zgenom ohmyzsh --completion plugins/golang
    zgenom ohmyzsh --completion plugins/taskwarrior

    zgenom load MenkeTechnologies/zsh-expand
    zgenom load sgpthomas/zsh-up-dir # Auto-bound to Ctrl-h
    zgenom load kevinywlui/zlong_alert.zsh
    zgenom load smeagol74/zsh-fzf-pass # Aliased to  fzp

    zgenom load zsh-users/zsh-autosuggestions
    zgenom load zdharma-continuum/fast-syntax-highlighting
    zgenom load zsh-users/zsh-syntax-highlighting 
    zgenom load zsh-users/zsh-completions
    zgenom load MenkeTechnologies/zsh-more-completions
    zgenom load --completion ianmkenney/timewarrior_zsh_completion

    zgenom save
    zgenom compile $ZDOTDIR
fi

