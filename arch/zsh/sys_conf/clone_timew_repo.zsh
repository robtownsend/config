clone_timew_repo() {
  local dir="$HOME/.timewarrior"
  local src="$HOME/dev/conf/timew"
  local repo="git@bitbucket.org:robtownsend/timew.git"
  local pass_entry="git_crypt/timew"

  if [[ -L "$dir" ]]; then
    return 0
  fi

  if [[ -e "$dir" ]]; then
    echo "Removing $dir"
    rm -rf "$dir"
  fi

  git clone "$repo" "$src"
  ln -s "$src" "$dir"
  repo_crypt --dir "$dir" --pass-key "$pass_entry"
}

clone_timew_repo
