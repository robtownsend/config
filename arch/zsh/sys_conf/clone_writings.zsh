clone_writings() {
  local dir=${WRITINGS_DIR:=$HOME/data/writings}
  local repo="git@bitbucket.org:robtownsend/writings.git"
  local pass_entry="git_crypt/writings"

  if [[ -d "$dir" ]]; then
    return 0
  fi

  git clone "$repo" "$dir"
  repo_crypt --dir "$dir" --pass-key "$pass_entry"
}

clone_writings
