daemonize_reminders() {
  source "/home/rob/dev/conf/dots/lib/create_daemon.sh"

  local dir="/home/rob/dev/conf/reminders"
  local cmd_pattern="remind -z .*\.rem$"
  local cmd="remind -z $dir/root.rem"
  local opts_file="$dir/daemon_opts.conf"

  if pgrep -f "$cmd_pattern" > /dev/null; then
    return 0
  fi

  if [[ ! -d "$dir" ]]; then
    return 0
  fi

  if [[ ! -f "$opts_file" ]]; then
    echo "reminder daemon opts file not found"
    return 1
  fi

  echo "Creating background proc for reminders"
  create_daemon "timed_reminders" "$cmd" "$opts_file"
}

daemonize_reminders
