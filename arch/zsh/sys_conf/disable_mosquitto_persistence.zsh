disable_mosquitto_persistence() {
  local setting_enabled="persistence true"
  local setting_disabled="persistence false"
  local settings_file="/etc/mosquitto/mosquitto.conf"

  if grep -q "$setting_disabled" "$settings_file"; then
    return 0
  fi

  echo "Modifying mosquitto settings"
  sudo sed -i "s/$setting_enabled/$setting_disabled/" "$settings_file"
  sudo service mosquitto restart
}

disable_mosquitto_persistence
