symlink_reminders() {
  local reminders_dir="$HOME/dev/conf/reminders"
  local reminders_file="$HOME/.reminders"
  local repo="git@bitbucket.org:robtownsend/reminders.git"
  local pass_entry="git_crypt/reminders"

  if [[ -L "$reminders_file" && -d "$reminders_dir" ]]; then
    return 0
  fi

  if [[ -e "$reminders_file" && ! -L "$reminders_file" ]]; then
    rm "$reminders_file"
  fi

  if [[ ! -d "$reminders_dir" ]]; then
    git clone "$repo" "$reminders_dir"
  fi

  if [[ ! -L "$reminders_file" ]]; then
    if [[ -f "$reminders_file" ]]; then
      rm "$reminders_file"
    elif [[ -d "$reminders_file" ]]; then
      rm -r "$reminders_file"
    fi

    ln -s \
      "$(realpath "$reminders_dir")" \
      "$reminders_file"
  fi

  repo_crypt \
    --dir "$reminders_dir" \
    --pass-key "$pass_entry"
}

symlink_reminders
