taskd_connect() {
  source "$HOME/dev/conf/dots/lib/utils/sudo_write.sh"

  local ssh_host="root@taskd"
  local ssh_key_file="$HOME/.ssh/taskd/taskd_root"
  local server_auth_dir="/home/taskd/taskddata/pki"
  local local_auth_dir="$HOME/.task/auth"
  local taskd_credentials_file="$local_auth_dir/taskd_connect"

  get_ssh_key() {
    mkdir -p $(dirname $ssh_key_file)

    if [[ ! -e "$ssh_key_file" ]]; then
      pass taskd/root_ssh_private_key > "$ssh_key_file"
      chmod 600 "$ssh_key_file"
    fi

    if [[ ! -e "$ssh_key_file.pub" ]]; then
      pass taskd/root_ssh_public_key > "$ssh_key_file.pub"
      chmod 600 "$ssh_key_file.pub"
    fi
  }

  get_user_key() {
    # Assumes only one user
    ssh \
      -i "$ssh_key_file" \
      "$ssh_host" \
      "ls /home/taskd/taskddata/orgs/Main/users" \
      | tr -d '[:blank:]'
  }

  copy_user_credentials() {
    key=$(get_user_key)

    mkdir -p "$local_auth_dir"

    files=(ca.cert.pem rob.cert.pem rob.key.pem)
    for f in "${files[@]}" ; do
      if [[ ! -e "$local_auth_dir/$f" ]]; then
        scp \
          -i "$ssh_key_file" \
          "$ssh_host:$server_auth_dir/$f" \
          "$local_auth_dir"
      fi
    done

    echo "taskd.server=$(pass taskd/server_url)" > "$taskd_credentials_file"
    echo "taskd.credentials=Main/rob/$key" >> "$taskd_credentials_file"
  }

  init_sync() {
    task sync init
    task sync
  }

  update_supervisor() {
    sudo supervisorctl reread
    sudo supervisorctl update
  }

  setup_sync_consumer() {
    local supervisor_conf=<<'EOF'
command=/home/rob/.config/taskwarrior/sync_consumer.sh
autostart=true
autorestart=true
EOF
    local dest="/etc/supervisor/conf.d/taskwarrior_sync_consumer.conf"

    echo "$supervisor_conf" | sudo_write "$dest"

    update_supervisor
  }

  if [[ -f "$taskd_credentials_file" ]]; then
    return 0
  fi

  if [[ ! -L "$HOME/.config/taskwarrior" ]]; then
    echo "Taskwarrior config directory not found"
    return 0
  fi

  get_ssh_key
  copy_user_credentials
  setup_sync_consumer
  init_sync
}

taskd_connect
