#!/usr/bin/env zsh

blackhole() {
  local cmd=$@

  eval $cmd >/dev/null 2>&1
}
