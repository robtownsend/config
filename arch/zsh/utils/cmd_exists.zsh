#!/usr/bin/env zsh

cmd_exists() {
  local cmd="$1"

  command -v "$cmd" &> /dev/null
}
