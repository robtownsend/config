#!/usr/bin/env zsh

function get_help_text {
  local input="$@"
  local cmd

  if [[ "$1" == "go" ]]; then
    shift
    cmd="go help $@"
  elif blackhole "$input --help"; then
    cmd="$input --help"
  elif blackhole "$input help"; then
    cmd="$input help"
  fi

  eval "$cmd" \
    | $PAGER \
      --language=help \
      --theme='Monokai Extended'
}
