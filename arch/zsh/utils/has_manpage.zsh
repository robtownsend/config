#!/usr/bin/env zsh

has_manpage() {
  blackhole "man $@"
}
