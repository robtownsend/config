#!/usr/bin/env zsh

load_cmd_completion() {
  local cmd=$1
  local generate_cmd=$2

  local completion_dir=$HOME/.local/share/zsh/completions
  local completion_file=$completion_dir/_$cmd

  mkdir -p $completion_dir

  if [[ -z $(cat $completion_file 2>/dev/null) ]]; then
    eval "$generate_cmd" > $completion_file
  fi

  load_completion_file "$cmd" "$completion_file"
}
