#!/usr/bin/env zsh

load_completion_file() {
  local cmd="$1"
  local file="$2"

  autoload -Uz $file
  _comps[$cmd]=_$cmd
}
