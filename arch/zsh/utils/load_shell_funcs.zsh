#!/usr/bin/env zsh

function load_shell_funcs {
  fpath=(
    $ZDOTDIR/zfuncs
    "${fpath[@]}"
  )
  # Lazy load all functions in zfuncs dir
  autoload -Uz $fpath[1]/*(.:t)
}
