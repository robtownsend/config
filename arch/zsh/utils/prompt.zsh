#!/usr/bin/env zsh

prompt() {
  local msg="$1"

  read "?$msg"
  
  echo "$REPLY"
}
