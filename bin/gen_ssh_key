#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/utils/parse_args.sh"
source "/home/rob/dev/conf/dots/lib/utils/prompt.sh"

alg="ed25519"
SSH_BASE_DIR="$HOME/.ssh"
dest_dir=""

declare -A opts=(
  ["name"]=""
  ["location"]=""
)
parse_args "opts" "$@"

name=${opts["name"]}
location=${opts["location"]}

while [[ -z $name ]]; do
  name=$(prompt "Key name?")
done

if [[ -z "$location" ]]; then
  location=$(prompt 'Key location? (relative to ~/.ssh)')
fi

if [[ -z "$location" ]]; then
  echo "No location provided. Using base ssh dir"
  dest_dir="$SSH_BASE_DIR"
else
  dest_dir="$SSH_BASE_DIR/$location"
fi

if [[ -e "$dest_dir/$name" ]]; then
  exit 0
fi

mkdir -p "$dest_dir"
ssh-keygen \
  -t "$alg" \
  -N "" \
  -f "$dest_dir/$name" \
  -C "$name - $(date -I)"
