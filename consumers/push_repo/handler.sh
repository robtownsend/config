#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/notify.sh"
source "/home/rob/dev/conf/dots/lib/utils/git_top_level_dir.sh"
repo="$1"

if [[ "$repo" == "null" ]]; then
  exit 0
fi

cd "$repo"
if ! git push; then
  notify "Error pushing $(git_top_level_dir)"
fi
