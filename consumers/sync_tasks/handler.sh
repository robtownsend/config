#!/usr/bin/env bash

# Here, supervisor redirects both stdout+stderr to a log file
# ~/.log/task_sync_out_stream.log, so any seemingly pointless cmds
# are actually being logged there

source "/home/rob/dev/conf/dots/lib/notify.sh"

sync() {
  zsh --login -c 'task sync'
}

date +'%F %r' # Timestamp sync results
if ! sync; then
  notify "Failed to sync taskwarrior"
fi

echo ""
