#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/notify.sh"
source "/home/rob/dev/conf/dots/lib/utils/network_connected.sh"

dir="/home/rob/.timewarrior"

if [[ ! -L "$dir" ]]; then
  notify "Error in timew conf; directory $dir is not a symlink"
  exit 0
fi

cd $dir

output=$(git status -s)
if [[ -z "$output" ]]; then
  exit 0
fi

# Only check against data files, otherwise editing a config file or anything else would
# generate a ton of commits

has_changes=false
readarray -t files < <(echo "$output")
for f in "${files[@]}"; do
  if [[ "$f" =~ data/.*$ ]]; then
    has_changes=true
    break
  fi
done

if ! $has_changes; then
  exit 0
fi

git add data

if ! zsh -l -c 'git commit -m "Auto-committing time tracking changes"'; then
  notify 'Error committing to timew repo'
  exit 0
fi

if ! network_connected; then
  exit 0
fi

if ! git push; then
  notify 'Error pushing timew repo'
  exit 0
fi
