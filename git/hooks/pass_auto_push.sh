#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/fire.sh"

repo="/home/rob/.password-store"

# Run via post-commit hook in the pass-store repo
fire "consumers.push_repo" "$repo"
