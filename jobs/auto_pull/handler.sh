#!/usr/bin/env bash

source "/home/rob/dev/conf/dots/lib/notify.sh"
source "/home/rob/dev/conf/dots/lib/utils/network_connected.sh"
source "/home/rob/dev/conf/dots/lib/sh/utils/get_current_git_branch.sh"

prune_branches_except() {
  local branch="$1"
  local repo="$2"
  local non_target_branches

  non_target_branches=$(git branch | grep -v "$branch") || true

  if [[ -z "$non_target_branches" ]]; then
    return 0
  fi

  if ! echo "$non_target_branches" | xargs git branch -d; then
    notify "Error in pruning repo $repo"
  fi
}

pull_dir() {
  local dir
  local base
  local pull_output
  local branch
  dir="$1"
  base=$(basename "$dir")

  cd "$dir"

  branch=$(get_current_git_branch)
  prune_branches_except "$branch" "$base"

  if ! pull_output=$(git pull 2>&1); then
    notify "Error in pulling repo ${base#.}: $pull_output" "auto_pull"
  fi
}

if ! network_connected; then
  exit 0
fi

job_dir=$(dirname "$0") # Cron job uses full path, so realpath step is unneeded
readarray -t dirs < "$job_dir/dirs"

for dir in "${dirs[@]}"; do
  pull_dir "$dir"
done
