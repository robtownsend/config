#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/utils/network_connected.sh"
source "/home/rob/dev/conf/dots/lib/fire.sh"

if ! network_connected; then
  exit 0
fi

fire_with_ack "consumers.sync_tasks"
