#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/notify.sh"
source "/home/rob/dev/conf/dots/lib/fire.sh"

attempt_task_sync() {
  fire_with_ack "consumers.sync_tasks"
}

gen_agenda_file() {
  if ! $agenda_executable "plain-text" "alerts" > "$todays_agenda_filename"; then
    notify "Failed generating agenda"
    exit 1
  fi

  $auto_version_executable \
    "$todays_agenda_filename" \
    "no-edit" \
    "no-confirm"
}

gen_image_from_agenda() {
  pango-view \
    --font mono \
    --no-display \
    --output "$todays_agenda_image" \
    "$todays_agenda_filename"
}

export TASKRC="/home/rob/.config/taskwarrior/taskrc"

agenda_executable="/home/rob/dev/conf/dots/bin/agenda"
auto_version_executable="/home/rob/dev/conf/dots/bin/auto_version"
notify_device_executable="/home/rob/dev/conf/dots/bin/notify_device"

writings_dir=${WRITINGS_DIR:=/home/rob/data/writings}

agendas_dir="$writings_dir/agendas"
yr="$(date +'%Y')"
month="$(date +'%m')"
yr_month_day="$(date +'%F')"
base_dir="$agendas_dir/$yr/$month"
todays_agenda_filename="$base_dir/${yr_month_day}.txt"
todays_agenda_image="$base_dir/${yr_month_day}.png"

mkdir -p "$(dirname "$todays_agenda_filename")"
attempt_task_sync

if [[ ! -f "$todays_agenda_filename" ]]; then
  gen_agenda_file
fi

if [[ ! -f "$todays_agenda_image" ]]; then
  gen_image_from_agenda
fi

cd "$base_dir"

$notify_device_executable \
  "$yr_month_day Agenda" \
  --title "Todays Agenda" \
  --attachment "$(basename "$todays_agenda_image")" \
  --device "zenfone"
