#!/usr/bin/env bash

source "/home/rob/dev/conf/dots/lib/notify.sh"
monitor_broker_executable="/home/rob/dev/conf/dots/bin/monitor_broker"

send_err_msg() {
  local err_msg="Healthcheck failed for mqtt broker"
  local msg_src="mqtt_broker_monitor"

  notify "$err_msg" "$msg_src"
}

if ! eval $monitor_broker_executable; then
  send_err_msg
fi
