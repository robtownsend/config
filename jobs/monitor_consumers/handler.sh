#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/notify.sh"
source "/home/rob/dev/conf/dots/lib/utils/join_arr.sh"

# Checks for a process in which the broker listens to a topic for
# each separate consumer
# Assumes a standard format for topics: consumers/$consumer_name/fire
consumers_dir="/home/rob/dev/conf/dots/consumers"

missing=()
for dir in "$consumers_dir"/*; do
  consumer_name=$(basename "$dir")
  topic="consumers/$consumer_name/fire"

  if ! pgrep --full "mosquitto_sub --topic $topic" > /dev/null; then
    missing+=( "$consumer_name" )
  fi
done

if [[ "${#missing[@]}" -gt 0 ]]; then
  notify "Listener not found for consumers $(join_arr "${missing[@]}")" "monitor_consumers"
fi
