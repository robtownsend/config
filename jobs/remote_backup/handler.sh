#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/notify.sh"
source "/home/rob/dev/conf/dots/lib/utils/host_is_reachable.sh"

borg_dir="/home/rob/dev/conf/backup/borg-backup"
executable="$borg_dir/remote-backup"
declare -A dests=(
  ["prem"]="borg-server"
  ["mirror"]="borg-mirror"
)

notify "Borg backup started; avoid modifying files if possible"

for dest in "${!dests[@]}"; do
  host=${dests["$dest"]}

  if ! host_is_reachable "$host"; then
    notify "Cannot reach $host for backup; Cannot proceed"
    continue
  fi

  $executable "$dest" -c || true
done

notify "Completed remote backups"
