#!/usr/bin/env bash
set -e

conf_file="/etc/anacrontab"
source "/home/rob/dev/conf/dots/lib/utils/line_number_of_match.sh"
source "/home/rob/dev/conf/dots/lib/utils/sudo_write.sh"

add_anacron_job() {
  local job="$1"
  local header_end_line
  local header_content
  local jobs_list

  if [[ ! -s "$conf_file" || -z $(cat "$conf_file") ]]; then
    header_content=""
    jobs_list="$job"
  else
    header_end_line=$(line_number_of_match "These replace cron's entries" < $conf_file)

    if [[ -z "$header_end_line" ]]; then
      header_content=""
      jobs_list="$job"
    else
      header_content=$(head -n "$header_end_line" "$conf_file")
      jobs_list=$(\
        (tail -n +"$((header_end_line + 1))" "$conf_file"; echo "$job") \
        | sort -u \
      )
    fi
  fi

  if [[ -z "$header_content" ]]; then
    echo "$jobs_list" | sudo_write "$conf_file"
  else
    echo "$header_content" | sudo_write "$conf_file"
    echo "$jobs_list" | sudo_append "$conf_file"
  fi
}
