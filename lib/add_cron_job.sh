#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"
source "/home/rob/dev/conf/dots/lib/utils/in_array.sh"
source "/home/rob/dev/conf/dots/lib/utils/join_arr.sh"
source "/home/rob/dev/conf/dots/lib/utils/prompt_loop.sh"
source "/home/rob/dev/conf/dots/lib/sh/utils/all_are_equal.sh"
source "/home/rob/dev/conf/dots/lib/sh/utils/index_of.sh"
source "/home/rob/dev/conf/dots/lib/sh/utils/push.sh"

initialize_job_list_for_user() {
  local job_list
  job_list=$(crontab -l 2>&1) || true

  if [[ "$job_list" == "no crontab for $USER" ]]; then
    echo '' | crontab -
  fi
}

get_crontab_fields() {
  local field="$1"

  case "$field" in
    freqs)
      crontab -l | tail -n+2 | sed -E -e 's/^[[:blank:]]+//' -e 's/[[:blank:]]+/ /g' | cut -d " " -f-5
      ;;
    handlers)
      crontab -l | tail -n+2 | sed -E -e 's/^[[:blank:]]+//' -e 's/[[:blank:]]+/ /g' -e 's/#.*//' | cut -d " " -f6-
      ;;
    tags)
      crontab -l | tail -n+2 | awk 'split($0, arr, "#") { print arr[2]}' | sed -E 's/[[:blank:]]//g'
      ;;
    *)
      return 1
      ;;
  esac 
}

get_delimited_crontab() {
  local -n jobs_tsv_ref="$1"

  # We're going to take these arrays and zip them together separated by a delimiter
  readarray -t freqs    < <(get_crontab_fields "freqs")
  readarray -t handlers < <(get_crontab_fields "handlers")
  readarray -t tags     < <(get_crontab_fields "tags")

  if ! all_are_equal "${#freqs[@]}" "${#handlers[@]}" "${#tags[@]}"; then
    print_to_stderr "Error parsing existing cron jobs: list of components aren't equal: ${#freqs[@]}, ${#handlers[@]}, ${#tags[@]}"
    return 1
  fi

  for i in "${!handlers[@]}"; do
    local current_freq="${freqs[$i]}"
    local current_cmd="${handlers[$i]}"
    local current_tag="${tags[$i]}"

    current_freq_tsv=$(echo "$current_freq" | sed -E 's/[[:space:]]+/DELIM/g')
    jobs_tsv_ref+=( "$current_freq_tsv${delimiter}${current_cmd}${delimiter}# $current_tag" )
  done
}

add_cron_job() {
  local awk_file="/home/rob/dev/conf/dots/lib/awk/columnize_crontab.awk"
  local current_freq="$1"
  local current_cmd="$2"
  local tag="$3"
  local delimiter="DELIM"
  local freqTsv
  local job
  local ans
  local idx

  declare -A vals=(
    ["current_freq"]="$current_freq"
    ["current_cmd"]="$current_cmd"
    ["tag"]="$tag"
  )

  for k in "${!vals[@]}"; do
    local v=${vals["$k"]}

    if [[ -z "$v" ]]; then
      print_to_stderr "add_cronjob: Missing value for $k"
      return 1
    fi
  done

  freqTsv=$(echo "$current_freq" | sed -E 's/[[:blank:]]+/DELIM/g')
  job="${freqTsv}${delimiter}${current_cmd}${delimiter}# ${tag}"

  initialize_job_list_for_user

  declare -a jobs_tsv
  get_delimited_crontab jobs_tsv
  readarray -t tags < <(get_crontab_fields "tags")

  # replace entry if it matches, otherwise append
  if idx=$(index_of "$tag" "${tags[@]}"); then
    jobs_tsv["$idx"]="$job"
  else
    push "jobs_tsv" "$job"
  fi

  printf "%s\n" "${jobs_tsv[@]}" \
    | awk -f "$awk_file" \
    | crontab -
}
