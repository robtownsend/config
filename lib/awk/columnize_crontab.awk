function printLeftMargin(isHeading) {
  printf "%-" leftMargin "s", isHeading ?
    "#" :
    " "
}

function print_headings(headings, widths) {
  printLeftMargin(1)

  for (i in headings)
    printf "%-" widths[i] "s  ", headings[i]
}

function print_col(str, width, padding) {
  printf "%-" width "s" padding, str
}

BEGIN {
  FS = "DELIM"

  leftMargin = 2

  split("min, hr, dom, mth, dow, handler, tag", headings, ", ")
  for (i in headings) {
    # We know each col will be at least as wide as its heading
    col_widths[i] = length(headings[i])
  }
}

{
  for (i = 1; i <= NF; i++) {
    # save each row
    data[NR, i] = $i

    len = length($i)

    if (len > col_widths[i])
      col_widths[i] = len
  }
}

END {
  print_headings(headings, col_widths)
  printf "\n"

  for (i = 1; i <= NR; i++) {
    for (j = 1; j <= length(headings); j++) {
      if (j == 1)
        printLeftMargin(0)
      print_col(data[i, j], col_widths[j], "  ")
    }

    printf "\n"
  }
}
