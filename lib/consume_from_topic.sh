#!/usr/bin/env bash
set -e

# May eventually use supervisor signals for err handling, but for now this will do
source "/home/rob/dev/conf/dots/lib/notify.sh"
source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"

can_connect_to_broker() {
  local executable="/home/rob/dev/conf/dots/bin/monitor_broker"

  $executable
}

consume_from_topic() {
  local topic="$1"
  local handler="$2"

  if [[ -z "$topic" ]]; then
    print_to_stderr "No value given for consumer topic"
    notify "Error in topic consumer!" "$(caller -1)"
    exit 1
  fi

  if [[ ! -x "$handler" ]]; then
    print_to_stderr "handler is either not present or executable"
    notify "Error in consumer for topic $topic: Handler is not executable" "$(caller -1)"
    exit 2
  fi

  if ! can_connect_to_broker; then
    print_to_stderr "Could not subscribe listener to topic: $topic"
    notify "Could not subscribe listener to topic: $topic" "consume_from_topic"
    exit 3
  fi

  # Packets must always have a message attached. If it must be empty or there's
  # no data to transmit, the string "null" will by convention be treated as
  # equivalent
  while read -r msg; do
    "$handler" "$msg"
  done < <(mosquitto_sub --topic "$topic")
}
