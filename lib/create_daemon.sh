#!/usr/bin/env bash

source "/home/rob/dev/conf/dots/lib/utils/sudo_write.sh"
source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"

supervisor_conf_dir="/etc/supervisor/conf.d"

get_supervisor_config() {
  local name="$1"
  local cmd="$2"
  local extra_options_file="$3"
  local extra_options
  extra_options=$(cat "$extra_options_file")

  cat << EOF
[program:$name]
command=$cmd
$extra_options
EOF
}

write_supervisor_conf() {
  local name=$1
  local cmd=$2
  local extra_options_file="$3"
  local runner_conf_file="$supervisor_conf_dir/${name}.conf"

  local supervisor_conf
  supervisor_conf=$(get_supervisor_config "$name" "$cmd" "$extra_options_file")

  echo "$supervisor_conf" \
    | sudo_write "$runner_conf_file"
}

update_supervisor() {
  sudo supervisorctl reread
  sudo supervisorctl update
}

create_daemon() {
  local consumer_name=$1
  local command=$2
  local extra_opts_file="$3"

  if [[ "$#" -lt 2 ]]; then
    print_to_stderr "define_supervisor_job: Not enough args"
    return 1
  fi

  echo "Writing config to supervisor..."
  write_supervisor_conf "$consumer_name" "$command" "$extra_opts_file"

  echo "Updating supervisor..."
  update_supervisor
  echo "Done."
}
