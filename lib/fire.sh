#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/notify.sh"
source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"
source "/home/rob/dev/conf/dots/lib/get_topic.sh"

check_topic_path() {
  local path="$1"

  ! [[ "$path" =~ .*\.fire ]]
}

fire_with_qos() {
  local topic_path="$1"
  local qos="$2"
  local topic
  shift
  shift
  msg=$*

  if ! topic=$(get_topic "$topic_path"); then
    print_to_stderr "Failed to get topic path"
    notify "Failed to get topic path"
    return 2
  fi

  if [[ $# -eq 0 || $msg == "" ]]; then
    msg="null"
  fi

  mosquitto_pub \
    --topic "$topic" \
    --message "$msg" \
    --qos "$qos"
}

fire() {
  local topic_path="$1"
  shift

  if ! check_topic_path "$topic_path"; then
    print_to_stderr "Topic path param should not include the fire signal"
    notify "Topic path param should not include the fire signal"
    return 1
  fi

  fire_with_qos "${topic_path}.fire" 0 "$@"
}

fire_with_ack() {
  local topic_path="$1"
  shift

  if ! check_topic_path "$topic_path"; then
    print_to_stderr "Topic path param should not include the fire signal"
    notify "Topic path param should not include the fire signal"
    return 1
  fi


  fire_with_qos "${topic_path}.fire" 2 "$@"
}
