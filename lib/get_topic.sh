#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"
source "/home/rob/dev/conf/dots/lib/sh/get_var.sh"

get_topic() {
  local topics_file="topics"
  local path="$1"

  get_var "$topics_file" "$path"
}
