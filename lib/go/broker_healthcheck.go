package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"sync"
	"time"
)

// Requires mosquitto to have persistent set to false

var topic = "healthcheck/mqtt/broker"

func run_topic_listener(ready chan string) (chan error, *bytes.Buffer, *bytes.Buffer) {
  var stdout bytes.Buffer
  var stderr bytes.Buffer

  done := make(chan error)

  go func() {
    defer close(done)

    // Exit after receiving only 1 message
    cmd := exec.Command("mosquitto_sub", "--topic", topic, "-C", "1")
    cmd.Stdout = &stdout
    cmd.Stderr = &stderr
    err := cmd.Start()
    if (err != nil) {
      fmt.Printf("Error starting subscribe command: %v\n", err.Error())
      panic(err.Error())
    }
    ready<-""
    err = cmd.Wait()

    if (err != nil) {
      fmt.Printf("Error received from subscribe command: %s\n", err.Error())
      done <-err
    } else {
      done <-nil
    }
  }()

  return done, &stdout, &stderr
}

func publish(wg *sync.WaitGroup) {
  defer wg.Done()

  cmd := exec.Command("mosquitto_pub", "--topic", topic, "--message", "heloooo", "--protocol-version", "5", "--qos", "2")
  err := cmd.Start()
  if (err != nil) {
    fmt.Printf("Error starting command: %v\n", err)
    panic(err.Error())
  }
  err = cmd.Wait()
  if (err != nil) {
    fmt.Printf("Error running publish command: %v\n", err)
    panic(err.Error())
  }
}

func main() {
  var wg sync.WaitGroup
  ready := make(chan string)

  done, _, _ := run_topic_listener(ready)
  <-ready

  wg.Add(1)
  go publish(&wg)
  go func() {
    wg.Wait()
  }()

  select {
  case err := <-done:
    if (err != nil) {
      fmt.Println("Err in broker heartbeat")
      os.Exit(1)
      panic(err.Error())
    }

    os.Exit(0)
  case <-time.After(3 * time.Second):
    fmt.Println("Broker healthcheck timed out")
    os.Exit(2)
  }
}
