def from_tw_date:
  . | strptime("%Y%m%dT%H%M%SZ") | todate;
