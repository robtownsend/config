#!/usr/bin/env bash

notify_cmd='notify-send'
log_file="/home/rob/.log/user_notifications.log"

# Calling notify-send from a background job like cron or supervisor doesn't work,
# So here we define the necessary env vars + log the notification to a log file
notify() {
  msg="$1"
  local msg_source="$2"
  uid=$(id -u)
  export DISPLAY=:0 
  export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus

  mkdir -p "$(dirname "$log_file")"

  if [[ -n "$msg_source" ]]; then
    msg_source="($msg_source):"
  fi

  date +'%F %r' >> "$log_file"
  printf "%s%s\n\n" "$msg_source" "$msg" >> "$log_file"

  $notify_cmd "$msg_source $msg"

  unset -v \
    DISPLAY \
    DBUS_SESSION_BUS_ADDRESS
}
