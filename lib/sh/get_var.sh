#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"

get_var() {
  local vars_dir="/home/rob/dev/conf/dots/lib/vars"
  local defs_file="$vars_dir/${1}.json"
  local path="$2"
  local jq="/usr/bin/jq"
  local var

  if [[ -z "$path" ]]; then
    print_to_stderr "No var path provided"
    return 1
  fi

  if [[ ! -f "$defs_file" ]]; then
    print_to_stderr "Var defs file not found!"
    return 2
  fi

  var=$($jq -r ".${path}" "$defs_file")
  if [[ -z "$var" || "$var" == "null" ]]; then
    print_to_stderr "The value at $path is invalid: $var"
    return 3
  fi

  echo "$var"
}
