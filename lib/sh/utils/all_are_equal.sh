#!/usr/bin/env bash

all_are_equal() {
  local args=( "$@" )
  local prev
  local curr

  # So we can compare all adjacent values + first w/ the last
  local first=${args[0]}
  args+=("$first")

  for idx in "${!args[@]}"; do
    if [[ $idx -eq 0 ]]; then
      continue
    fi

    prev=${args[(( $idx - 1 ))]}
    curr=${args["$idx"]}

    if [[ $prev != "$curr" && $prev -ne $curr ]]; then
      return 1
    fi
  done
}
