#!/usr/bin/env bash

source "/home/rob/dev/conf/dots/lib/sh/utils/push.sh"
source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"

filter() {
  local -n filter_arr_ref="$1"
  local fn="$2"
  local -n dest_arr_filter_ref="$3"

  # shellcheck disable=2034
  dest_arr_filter_ref=()

  for elem in "${filter_arr_ref[@]}"; do
    if eval "$fn '$elem'"; then
      push dest_arr_filter_ref "$elem"
    fi
  done
}

filter_with_2() {
  local -n filter_arr_ref="$1"
  local fn="$2"
  local arg1="$3"
  local arg2="$4"
  local -n dest_arr_filter_ref="$5"
  local elem

  # shellcheck disable=2034
  dest_arr_filter_ref=()

  for i in "${!filter_arr_ref[@]}"; do
    elem=${filter_arr_ref[$i]}
    if eval "$fn '$arg1' '$arg2' '$elem'"; then
      # shellcheck disable=2034
      dest_arr_filter_ref[i]="$elem"
    fi
  done
}
