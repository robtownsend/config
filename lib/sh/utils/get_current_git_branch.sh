#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"

get_current_git_branch() {
  local dir
  dir=$(pwd)

  if [[ ! -d "$dir/.git" ]]; then
    print_to_stderr "Attempting to get branch of $dir, which is not a git repo"
    return 1
  fi

  git rev-parse --abbrev-ref HEAD
}
