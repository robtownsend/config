#!/usr/bin/env bash
set -e

get_device_path_by_model() {
  local model="$1"
  local f="/tmp/awk_parser.awk"

  cat <<- EOF > "$f"
    BEGIN { RS="" }
    (\$0 ~ "$model") {
      match(\$0, /(^[^:]+):/, arr)
      split(arr[1], tokens)
      print tokens[2]
      exit 0
    }
EOF


  awk -f "$f" < <(sudo fdisk -l)
}
