#!/usr/bin/env bash

index_of() {
  local needle="$1"
  shift
  local haystack=( "$@" )

  for i in "${!haystack[@]}"; do
    local h="${haystack[$i]}"

    if [[ "$needle" == "$h" ]]; then
      echo "$i"
      return 0
    fi
  done

  return 1
}
