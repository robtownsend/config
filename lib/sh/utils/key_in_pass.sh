#!/usr/bin/env bash
set -e

key_in_pass() {
  local pass_dir="/home/rob/.password-store"
  local key="$1"

  [[ -f "$pass_dir/$key.gpg" ]]
}
