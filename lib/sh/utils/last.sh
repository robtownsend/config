#!/usr/bin/env bash

last() {
  local arr=( "$@" )
  local length="${#arr[@]}"
  local idx=$(( length - 1 ))

  echo "${arr[$idx]}"
}
