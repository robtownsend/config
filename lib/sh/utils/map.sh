#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/sh/utils/push.sh"

map() {
  local -n arr_map_internal_ref="$1"
  local func="$2"
  local -n dest="$3"

  local map_result_internal=()

  for elem in "${arr_map_internal_ref[@]}"; do
    push map_result_internal "$(eval "$func '$elem'")"
  done
  
  # shellcheck disable=SC2034
  dest=()
  for v in "${map_result_internal[@]}"; do
    push dest "$v"
  done
}

# map_with() {
#   local arr_name="$1"
#   local func="$2"
#   local arg="$3"
#   shift
#   shift
#   shift
#   declare -a arr=( "$@" )
#   local result=""
#
#   for elem in "${arr[@]}"; do
#     result="$result$($func "$arg" "$elem")\t"
#   done
#
#   readarray -t -d $'\t' "$arr_name" \
#     < <(echo -en "$result")
# }
