#!/usr/bin/env bash

source "/home/rob/dev/conf/dots/lib/sh/utils/map.sh"

# will only work for flat arrays i think
map_json_array() {
  # shellcheck disable=SC2034
  local -n dest_ref_map_json_arr_internal="$1"
  local fn="$2"
  local json_arr="$3"

  local json_objs=()
  # shellcheck disable=SC2034
  readarray -t json_objs < <(echo "$json_arr" | jq -c '.[]') # print objs per line
  map "json_objs" "$fn" "dest_ref_map_json_arr_internal"
}
