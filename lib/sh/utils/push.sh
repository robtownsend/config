#!/usr/bin/env bash

source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"

push() {
  local -n arr_ref_push_internal="$1"
  local val="$2"
  local len="${#arr_ref_push_internal}"

  arr_ref_push_internal[len]="$val"
}
