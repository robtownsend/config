#!/usr/bin/env bash
set -e

cmd_exists() {
  local cmd="$1"

  command -v "$cmd" &> /dev/null
}
