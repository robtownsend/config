#!/usr/bin/env bash
set -e

copy_arr() {
  local -n src="$1"
  local -n dest="$2"

  for k in "${!src[@]}"; do
    # shellcheck disable=SC2034
    dest["$k"]=${src["$k"]}
  done
}
