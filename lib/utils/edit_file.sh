#!/usr/bin/env bash
set -e

edit_file() {
  local file="$1"
  local opt="$2"
  local save_all=false
  local terminal
  terminal=$(tty)

  if [[ "$opt" == "save-all" ]]; then
    save_all=true
  fi

  # shellcheck disable=SC2094
  AV_SAVE_ALL=$save_all $EDITOR "$file" <"$terminal" >"$terminal"
}
