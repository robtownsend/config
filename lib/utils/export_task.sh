#!/usr/bin/env bash
set -e

export_task() {
  local id="$1"

  task "$id" export | jq '.[0]'
}
