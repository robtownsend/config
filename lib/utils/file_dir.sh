#!/usr/bin/env bash
set -e

file_dir() {
  local file
  file=$(realpath "$1")

  dirname "$file"
}
