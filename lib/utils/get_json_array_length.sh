#!/usr/bin/env bash
set -e

get_json_array_length() {
  local json="$1"

  echo "$json" | jq 'length'
}
