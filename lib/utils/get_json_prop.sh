#!/usr/bin/env bash
set -e

get_json_prop() {
  local json="$1"
  local prop_name="$2"

  echo "$json" | jq -r ".$prop_name"
}
