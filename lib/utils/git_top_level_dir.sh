#!/usr/bin/env bash
set -e

git_top_level_dir() {
  git rev-parse --show-toplevel 2> /dev/null
}
