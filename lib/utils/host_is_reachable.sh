#!/usr/bin/env bash
set -e

host_is_reachable() {
  local host="$1"

  ping -c1 "$host" >/dev/null 2>&1
}
