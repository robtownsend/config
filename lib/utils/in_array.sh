#!/usr/bin/env bash
set +e #otherwise the script will exit on error

# https://stackoverflow.com/questions/3685970/check-if-a-bash-array-contains-a-value

in_array() {
  local match="$1"
  shift
  local arr=( "$@" )

  for elem in "${arr[@]}"; do
    if [[ "$elem" == "$match" ]]; then
      return 0
    fi
  done

  return 1
}

