#!/usr/bin/env bash
set -e

utils_dir="/home/rob/dev/conf/dots/lib/utils"

source "$utils_dir/git_top_level_dir.sh"

is_git_repo() {
  local dir
  dir=$(git_top_level_dir)

  [[ -n "$dir" ]] && [[ -d "$dir/.git" ]]
}
