#!/usr/bin/env bash

# Usage: $(join_arr "${arr[@]}")
join_arr() {
  local arr=( "$@" )
  local delim=',' # Must be single char

  (IFS=$delim; echo "${arr[*]}")
}

join_arr_with_delim() {
  local delim=$1
  shift

  local arr=( "$@" )
  local awk_str

  awk_str=$(cat <<'EOF'
  {
    for (i = 1;i <= NF; i++)
      a[i] = $i
  }

  END {
    str=a[1]
    for (i = 2; i <= NF; i++)
       str = str delim a[i]

    print str
  }
EOF
)

  echo "${arr[@]}" | awk -v "delim=$delim" "$awk_str"
}
