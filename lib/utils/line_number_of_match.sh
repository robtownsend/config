#!/usr/bin/env bash
set -e

line_number_of_match() {
  local pattern="$1"

  grep -n "$pattern" - \
    | cut -f1 -d:
}
