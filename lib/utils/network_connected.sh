#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/utils/host_is_reachable.sh"

network_connected() {
  local host="1.1.1.1"

  host_is_reachable "$host"
}
