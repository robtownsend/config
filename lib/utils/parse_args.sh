#!/usr/bin/env bash

source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"
source "/home/rob/dev/conf/dots/lib/utils/split_str.sh"
source "/home/rob/dev/conf/dots/lib/utils/join_arr.sh"

# Helpers

is_flag() {
  local arg="$1"

  [[ "$arg" =~ ^--(.+)$ || "$arg" =~ ^-([[:alnum:]])$ ]]
}

get_next() {
  local arg="$1"
  
  if is_flag "$arg"; then
    # shellcheck disable=SC2034
    echo "${BASH_REMATCH[1]}"
  else
    return 1
  fi
}

parse_cmdline_arg() {
  local token
  local key
  local -n vars_ref="$1"
  local -n aliases_ref="$2"
  shift
  shift

  if ! token=$(get_next "$1"); then
    print_to_stderr "parse_args: Error parsing: $1 is not a valid parameter"
    kill -s SIGTERM $$
  fi

  if [[ "${aliases_ref[$token]}" == "" ]]; then
    print_to_stderr "$token is not recognized as a parameter"
    kill -s SIGTERM $$
  fi

  key="${aliases_ref["$token"]}"
  shift
  next="$1"
  if is_flag "$next" || [[ -z "$next" ]]; then
    vars_ref["$key"]=true
  else
    # shellcheck disable=SC2034
    vars_ref["$key"]="$next"
    return 0
  fi

  return 1
}

# takes a key -> aliases map, returns a map of aliases -> key
# This also includes a mapping of the key to itself
# This makes the parsing step a simple lookup
mirror_aliases_map() {
  local -n vars_inner_ref="$1"
  local -n aliases_map_ref="$2"

  for key in "${!vars_inner_ref[@]}"; do
    local aliases=()
    local str=${vars_inner_ref["$key"]}

    aliases_map_ref["$key"]=$key

    split_str aliases "$str" ";"

    if [[ "${#aliases[@]}" -eq 0 ]]; then
      continue
    fi

    for alias in "${aliases[@]}"; do
      if [[ -z "$alias" ]]; then
        continue
      fi

      # shellcheck disable=SC2034
      aliases_map_ref["$alias"]="$key"
    done
  done
}

clear_vals() {
  local -n map_ref="$1"

  for key in "${!map_ref[@]}"; do
    map_ref["$key"]=""
  done
}

# Checks for:
# - pa_required
read_param_constraints() {
  local -n vars_ref_param_constraints="$1"
  local -n params_map_ref="$2"
  local val

  local valid_constraints=( pa_required )

  for var in "${!vars_ref_param_constraints[@]}"; do
    local directives=()
    local var_constraints=()
    local non_constraints=()

    val=${vars_ref_param_constraints["$var"]}
    split_str "directives" "$val" ";"

    for directive in "${directives[@]}"; do
      local is_constraint
      is_constraint=false

      for i in "${!valid_constraints[@]}"; do
        local c="${valid_constraints[$1]}"
        if [[ "$directive" == "$c" ]]; then
          is_constraint=true
          break
        fi
      done

      if [ $is_constraint = true ]; then
        var_constraints+=( "$directive" )
      else
        non_constraints+=( "$directive" )
      fi
    done

    # shellcheck disable=SC2034
    params_map_ref["$var"]=$(join_arr "${var_constraints[@]}")
    vars_ref_param_constraints["$var"]=$(join_arr_with_delim ";" "${non_constraints[@]}")
  done
}

validate_against_constraints() {
  local -n vars_validate_ref="$1"
  local -n param_constraints_ref="$2"
  declare -A failed_constraints

  for arg_name in "${!vars_validate_ref[@]}"; do
    constraints=()
    local constraints_str=${param_constraints_ref[$arg_name]}
    local arg_val=${vars_validate_ref[$arg_name]}

    split_str "constraints" "$constraints_str" ","

    if [[ "${#constraints[@]}" -eq 0 ]]; then
      continue
    fi

    local failed=()
    for c in "${constraints[@]}"; do
      case $c in
        pa_required)
          if [[ -z "$arg_val" ]]; then
            failed+=( "pa_required" )
          fi
          ;;
      esac
    done

    # shellcheck disable=SC2034
    failed_constraints[$arg_name]=$(join_arr "${failed[@]}")
  done

  check_constraint_results "failed_constraints"
}

check_constraint_results() {
  local -n constraints_results_ref="$1"
  local errors_encountered=0

  for k in "${!constraints_results_ref[@]}"; do
    local v="${constraints_results_ref[$k]}"

    if [[ -z "$v" ]]; then
      continue
    fi

    errors_encountered=1

    declare -a violations
    split_str "violations" "$v" ","

    for violation in "${violations[@]}"; do
      case "$violation" in
        pa_required)
          print_to_stderr "$k is required"
          ;;
      esac
    done
  done

  return $errors_encountered
}

# Main

# Usage:
#   parse_args
#     assoc arr of var names -> aliases (i.e backup -> "b;backup_files")
#     $@ (list of cmd line args)
#   returns assoc array of var names -> values
#
#   Technically, the list of aliases is overwritten and nothing is returned, cause that's how bash do it
parse_args() {
  if [[ -z "$1" ]]; then
    print_to_stderr "parse_args: No options map given"
    return 1
  fi

  # shellcheck disable=SC2034
  local -n vars="$1"
  shift

  # shellcheck disable=SC2034
  declare -A param_constraints
  read_param_constraints "vars" "param_constraints"

  # shellcheck disable=SC2034
  declare -A aliases_map
  mirror_aliases_map "vars" "aliases_map"
  clear_vals "vars"

  while [[ -n "$1" ]]; do
    # True indicates the value immediately following the flag was consumed,
    # so need an extra shift to compensate
    if parse_cmdline_arg "vars" "aliases_map" "$@"; then
      shift
    fi

    shift
  done

  validate_against_constraints "vars" "param_constraints"
}

# Error handling

sig_handler() {
  exit 3
}

trap sig_handler SIGTERM
