#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/utils/in_array.sh"

parse_flags() {
  local -n flags_map_ref="$1"
  local args=( "$@" )

  for arg in "${args[@]}"; do
    if in_array "$arg" "${!flags_map_ref[@]}"; then
      flags_map_ref["$arg"]=true
    fi
  done
}
