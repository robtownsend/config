#!/usr/bin/env bash
set -e

print_to_console() {
  local msg="$1"
  local console
  console=$(tty)

  echo "$msg" > "$console"
}
