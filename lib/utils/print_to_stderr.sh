#!/usr/bin/env bash

print_to_stderr() {
  local str="$1"

  echo >&2 "$str"
}
