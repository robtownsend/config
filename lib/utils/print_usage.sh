#!/usr/bin/env bash
set -e

source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"
source "/home/rob/dev/conf/dots/lib/utils/split_str.sh"

print_usage() {
  if [[ $# -lt 3 ]]; then
    print_to_stderr "print_usage: Not enough args"
    return 1
  fi

  local program_name="$1"
  local -n aliases_ref_map="$2"
  local -n usage_ref_map="$3"
  declare -a param_aliases
  local str

  print_to_stderr "$program_name:"
  for param in "${!usage_ref_map[@]}"; do
    print_to_stderr "- $param"

    str="${aliases_ref_map["$param"]}"
    param_aliases=()
    split_str param_aliases "$str" ";"

    for alias in "${param_aliases[@]}"; do
      print_to_stderr "  $alias"
    done

    print_to_stderr "    ${usage_ref_map["$param"]}"
  done
}
