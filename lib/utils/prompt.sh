#!/usr/bin/env bash
set -e

function prompt {
  local msg="$1 "

  read -rp "$msg"

  echo "$REPLY"
}
