#!/usr/bin/env bash
set -e

utils_dir="/home/rob/dev/conf/dots/lib/utils"
source "$utils_dir/prompt.sh"
source "$utils_dir/in_array.sh"
source "$utils_dir/print_to_stderr.sh"
source "$utils_dir/join_arr.sh"

# eg prompt_loop "prompt text?" "ans1" "ans2" etc
# or
# prompt_loop "prompt text?" "${arr[@]}"
prompt_loop() {
  local prompt_text="$1"
  shift
  local valid_answers=( "$@" )
  local ans

  if [[ "${#valid_answers[@]}" -eq 0 ]]; then
    valid_answers=(y n)
  fi

  while true; do
    ans=$(prompt "$prompt_text ")

    if in_array "$ans" "${valid_answers[@]}"; then
      break
    fi

    print_to_stderr "Answer must be one of: $(join_arr "${valid_answers[@]}")"
  done

  echo "$ans"
}
