#!/usr/bin/env bash
set -e

split_str() {
  local -n arr_ref="$1"
  local str="$2"
  local delim="$3"

  if [[ -z "$delim" ]]; then
    delim=","
  fi

  # shellcheck disable=SC2034
  readarray -t arr_ref \
    < <(echo "$str" | awk -v delim="$delim" 'BEGIN { RS = delim } {print $1}')
}
