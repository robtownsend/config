#!/usr/bin/env bash
set -e

string_contains() {
  local str="$1"
  local substr="$2"

  [[ "$str" =~ .*"$substr".* ]]
}
