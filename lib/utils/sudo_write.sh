#!/usr/bin/env bash

sudo_write() {
  sudo tee "$@" &> /dev/null
}

sudo_append() {
  sudo tee -a "$@" &> /dev/null
}
