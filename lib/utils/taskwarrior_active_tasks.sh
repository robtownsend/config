#!/usr/bin/env bash
set -e

export TASKRC="$HOME/.config/taskwarrior/taskrc"

taskwarrior_active_tasks() {
  task +ACTIVE export
}
