#!/usr/bin/env bash
set -e

truncate_str() {
  local str="$1"
  local cutoff=45

  if [[ "${#str}" -lt $cutoff ]]; then
    echo "$str"
  else
    echo "${str:0:$cutoff}..."
  fi
}
