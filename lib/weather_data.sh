#!/usr/bin/env bash
set -e

fulton_id="GAC121"

baseUrl="https://api.weather.gov"
alerts_url="${baseUrl}/alerts/active/area/GA"
forecast_url="${baseUrl}/gridpoints/FFC/51,91/forecast"

fetch_cmd="curl -s"

fetch_weather_alerts() {
  # sed step add newline between entries for easier reading
  $fetch_cmd "$alerts_url" \
    | jq ".features[].properties | select(.affectedZones | map(test(\"$fulton_id\")) | any)" \
    | jq '{ event: .event, areaDesc: .areaDesc, headline: .headline, response: .response, expires: .expires, category: .category, severity: .severity, certainty: .certainty, urgency: .urgency }' \
    | jq -s \
    | json2yaml \
    | sed '/^ *- /i\\'
}

csv_to_html_tbl() {
  echo '<table style="width: 100%">'

  sed \
    -e 's/^/<tr><td style="width: 20%">/' \
    -e 's/,/<\/td><td style="width: 20%">/g' \
    -e 's/$/<\/td><\/tr>/' \
    -e 's/"//g' \
    < <(cat -)

  echo "</table>"
}

get_forecast() {
  $fetch_cmd $forecast_url \
    | jq -r '
      .properties.periods[:4]
      | .[]
      | {
        name: .name,
        desc: (.detailedForecast ),
        windSpeed: .windSpeed,
        temp: (.temperature | tostring | . + " °F"),
        day: (.startTime | split("T") | .[0] + "T00:00:00Z" | fromdate | strftime("%m/%d"))
      }'
}

fetch_forecast() {
  local opt="$1"

  if [[ "$opt" == "html" ]]; then
    get_forecast \
      | jq -sr '.[] | [.day, .name, .temp, (.desc | gsub(","; "")), .windSpeed, .date] | @csv' \
      | csv_to_html_tbl
  else
    # That weird bit of perl is only printing the table border when it sees a date in the form m/d.
    # Because the forecast desc can be wrapped, we don't want to do that every line
    get_forecast \
      | jq -sr '.[] | [.day, .name, .temp, .desc, .windSpeed, .date] | @tsv' \
      | column \
        --output-width 95 \
        --output-separator "    " \
        --table \
        --separator $'\t' \
        --table-columns Date,Name,Temp,Desc,Wind \
        --table-wrap Desc \
        --table-noheadings \
      | perl -ne 'use feature qw(say); if (/\d\/\d/) {say ""; say "───────────────────────────────────────────────────────────────────────────────────────────────"; say "";}  print $_;'
  fi
}
