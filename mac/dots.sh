#!/bin/bash

set -e

config_dir="$HOME/dev/conf/dots"

declare -A dots=(
  ["git/gitconfig"]="$HOME/.gitconfig"
  ["arch/zsh"]="$HOME/.config/zsh"
  ["nvim"]="$HOME/.config/nvim"
  ["taskwarrior"]="$HOME/.config/taskwarrior"
  ["tmux"]="$HOME/.config/tmux"
  ["zshenv"]="$HOME/.zshenv"
)

for src in "${!dots[@]}"; do
  orig="$config_dir/$src"
  dest="${dots[$src]}"

  if [[ -e "$dest" || -L "$dest"  ]]; then
    echo "Running rm -rf $dest"
    rm -rf "$dest"
  fi

  echo "Running ln -s $orig $dest"
  ln -s "$orig" "$dest"
done
echo "Done configuring ~/.config"

echo "Done."

