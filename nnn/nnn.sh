export NNN_ARCHIVE="\\.(7z|bz2|gz|tar|tgz|zip)$"
export NNN_FCOLORS="c1e2272e006033f7c6d6ab01"
export NNN_FIFO="/tmp/nnn.fifo"
export NNN_OPENER="$HOME/.config/nnn/plugins/nuke"
export NNN_PLUG="z:zoxide;p:preview-tui;d:dragdrop;o:organize"
export NNN_TRASH=1 # trash instead of delete (needs trash-cli)
export NNN_BMS="c:~/dev/config"

