local load_skel = require('utils.load_skel')
local cmd_exists = require('utils.cmd_exists')

local function starts_with(str, start)
  return str:sub(1, #start) == start
end

local norg_general_skel = 'skel.norg.norg_general'
local journal_skel_filename = 'skel.norg.journal'
local tasknote_skel_filename = 'skel.norg.tasknote'
local tasknote_brainstorm_skel_filename = 'skel.norg.tasknote_brainstorm'

vim.wo.wrap = true
vim.wo.spell = true

local writings_dir = os.getenv("WRITINGS_DIR")
local journal_dir = writings_dir .. "/journal"
local tasknotes_dir = writings_dir .. "/tasknotes"
local brainstorms_dir = writings_dir .. "/tasknotes/brainstorms"
local current_dir = vim.fn.expand('%:p:h')
local skel

if (starts_with(current_dir, journal_dir)) then
  skel = journal_skel_filename
elseif (current_dir == brainstorms_dir) then
  skel = tasknote_brainstorm_skel_filename
elseif (current_dir == tasknotes_dir) then
  skel = tasknote_skel_filename
else
  skel = norg_general_skel
end

load_skel('*.norg', skel)

vim.cmd('autocmd VimEnter *.norg :ZenMode')
vim.api.nvim_create_autocmd(
  { "VimEnter", "BufReadPost" },
  {
    pattern = "*.norg",
    callback = function()
      if ( cmd_exists('ZenMode') ) then
        local zen_mode = require("zen-mode")

        zen_mode.open();
      end
    end
  }
)
