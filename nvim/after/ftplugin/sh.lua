local load_skel = require('utils.load_skel')
local skel_filename = 'skel.shell'

load_skel('*.sh', skel_filename)
