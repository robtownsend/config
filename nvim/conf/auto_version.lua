local save_file = "/tmp/auto_version_files.log"

function table.contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

if os.getenv("AV_SAVE_ALL") then
  local opened_files = {}

  vim.api.nvim_create_autocmd(
    { "BufEnter" },
    {
      callback = function()
        local file_path = vim.fn.expand("%:p")

        if file_path and #file_path > 0 and table.contains(opened_files, file_path) ~= true then
            table.insert(opened_files, file_path)
        end
      end
    }
  )

  vim.api.nvim_create_autocmd(
    { "VimLeave" },
    {
      callback = function()
        local file = io.open(save_file, "w")

        if (file == nil) then
          return
        end

        for _, file_path in ipairs(opened_files) do
          file:write(file_path .. "\n")
        end

        file:close()
      end
    }
  )
end
