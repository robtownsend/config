set nocompatible
syntax on
set nu
set nowrap
set noswapfile
set incsearch
set nohlsearch
set termguicolors

" set omnifunc=syntaxcomplete#Complete

" set foldminlines=10
" set foldmethod=expr
" set foldexpr=nvim_treesitter#foldexpr()

" tab = 2 spaces
set tabstop=2
set expandtab
set shiftwidth=2
set smartindent

" More natural splits
set splitbelow " Horizontal split below current
set splitright " Vertical split to right of current

" Automatically rebalance windows on frame resize
autocmd VimResized * :wincmd =

