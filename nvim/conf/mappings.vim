" Handle multiple indents of a visual block
vno > >gv
vno <tab> >gv
vno < <gv
vno <S-tab> <gv

" Easy insertion of a trailing ; or ,
nno ;; <Esc>A;<Esc>^
nno ,, <Esc>A,<Esc>^

" Resize windows with arrows
nno <C-Up> :resize +2<CR>
nno <C-Down> :resize -2<CR>
nno <C-Left> :vertical resize -2<CR>
nno <C-Right> :vertical resize +2<CR>

" Remap win prefix to one that doesn't close browser tabs
nno <C-M-w> <C-w>

tno <M-S-T> <c-\><c-n>:FloatermToggle<CR>
nno <M-S-t> :FloatermToggle<CR>
