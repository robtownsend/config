" todo add colemak-friendly bindings

" Smoother way to move between windows
""map <C-Left> <C-W>Left
"map <C-Right> <C-W>Right
"map <C-Up> <C-W>Up
"map <C-Down> <C-W>Down

" Switch windows in terminal mode
tnoremap <M-Left> <C-\><C-n><C-w>Left
tnoremap <M-Right> <C-\><C-n><C-w>Right
tnoremap <M-Up> <C-\><C-n><C-w>Up
tnoremap <M-Down> <C-\><C-n><C-w>Down
