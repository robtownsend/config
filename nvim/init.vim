let mapleader = "\<SPACE>"
let maplocalleader=','

set clipboard+=unnamedplus " Use +y to copy into sys clipboard

set showcmd " show leader key for the duration of the timeout
set hidden
set undofile

set runtimepath=~/.config/nvim,~/.config/nvim/conf,$VIMRUNTIME,~/.config/nvim/after

runtime conf/editor.vim
runtime conf/plugins.lua
runtime conf/motions.vim
runtime conf/mappings.vim
runtime conf/auto_version.lua
