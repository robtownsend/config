require("mason-lspconfig").setup {
  -- Uses lspconfig names, not mason names unfortunately
  -- https://github.com/williamboman/mason-lspconfig.nvim/blob/main/doc/server-mapping.md
  ensure_installed = {
    "awk_ls",
    "bashls",
    "cssls",
    "dockerls",
    "eslint",
    "gopls",
    "graphql",
    "helm_ls",
    "jsonls",
    "marksman",
    "perlnavigator",
    "purescriptls",
    "raku_navigator",
    "rubocop",
    "solargraph",
    "sqlls",
    "lua_ls",
    "terraformls",
    "ts_ls",
    "yamlls",
  },
  automatic_installation = true,
}
require("mason-lspconfig").setup_handlers {
  function (server_name)
      require("lspconfig")[server_name].setup {}
  end,
  -- Next, you can provide a dedicated handler for specific servers.
  -- For example, a handler override for the `rust_analyzer`:
  -- ["rust_analyzer"] = function ()
  --     require("rust-tools").setup {}
  ["lua_ls"] = function ()
    require("lspconfig").lua_ls.setup {
      settings = {
        Lua = {
          diagnostics = {
            globals = { "vim" }
          }
        }
      }
    }
  end,
  ["bashls"] = function()
    require("lspconfig").bashls.setup {
      filetypes = { "sh", "bash", "zsh", }
    }
  end,
}

