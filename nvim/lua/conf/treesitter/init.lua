require("nvim-treesitter.configs").setup {
  ensure_installed = 'all',
  autotag = { enable = true },
  highlight = require('conf.treesitter.modules.highlightOpts'),
  indent = require('conf.treesitter.modules.indentOpts'),
  matchup = require('conf.treesitter.modules.matchupOpts'),
  textobjects = require('conf.treesitter.modules.textObjects'),
  textsubjects = require('conf.treesitter.modules.textSubjects'),
  playground = require('conf.treesitter.modules.playground'),
}

require('conf.treesitter.folding')
