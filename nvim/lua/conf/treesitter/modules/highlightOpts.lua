local opts = {
  enable = true,
  additional_vim_regex_highlighting = true,
}

return opts
