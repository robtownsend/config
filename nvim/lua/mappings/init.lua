local cmd_exists = require('utils.cmd_exists')

vim.keymap.set(
  'n',
  ':',
  function()
    if ( cmd_exists('FineCmdline') ) then
      vim.cmd('FineCmdline')
    else
      vim.api.nvim_feedkeys(
        ':',
        'n',
        true
      )
    end
  end
)

require("mappings.leader")
require("mappings.localleader")
require("mappings.wincmd")
