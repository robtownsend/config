local keymap = {
  { '<leader>a', group = 'Alternates' },
  { '<leader>ab', '<Cmd>A<CR>', desc = 'buf' },
  { '<leader>as', '<Cmd>AS<CR>', desc = 'split' },
  { '<leader>at', '<Cmd>AT<CR>', desc = 'tab' },
  { '<leader>av', '<Cmd>AV<CR>', desc = 'vsplit' }
}

return keymap

