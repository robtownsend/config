local close_buffers = require('close_buffers')
local telescope = require('telescope.builtin')

local keymap = {
  { '<leader>b', group = 'Buffers' },
  {
    '<leader>bb',
    function ()
      close_buffers.delete({ type = 'nameless' })
      telescope.buffers()
    end,
    desc = 'buffers'
  },
  {
    '<leader>bc',
    function()
      -- Wipe all buffers except the current focused
      close_buffers.wipe({ type = 'other' })
    end,
    desc = 'clear'
  },
  {
    '<leader>bw',
    function ()
      -- Wipe all buffers
      close_buffers.wipe({ type = 'all', force = true })
    end,
    desc = 'wipe'
  },
}

return keymap

