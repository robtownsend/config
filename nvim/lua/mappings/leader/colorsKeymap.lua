local function toggle_bg()
  local bg = vim.o.background
  if (bg == 'light') then
    vim.o.background = 'dark'
  else
    vim.o.background = 'light'
  end
end

local keymap = {
  { '<leader>c', group = 'Colors' },

  {
    '<leader>cc',
    '<Cmd>Telescope colorscheme<CR>',
    desc = 'find'
  },
  {
    '<leader>cd',
    function ()
      vim.cmd([[
        set background=dark
        colorscheme tokyonight-night
      ]])
    end,
    desc = 'dark theme' },
  {
    '<leader>cg',
    function ()
      vim.cmd([[
        set background=light
        colorscheme gruvbox
      ]])
    end,
    desc = 'gruvbox light'
  },
  {
    '<leader>cs',
    function ()
      vim.cmd([[
        set background=light
        colorscheme NeoSolarized
      ]])
    end,
    desc = 'solarized light'
  },
  {
    '<leader>ct',
    function ()
      vim.cmd([[
        set background=light
        colorscheme tokyonight-day
      ]])
    end,
    desc = 'tokyo-day'
  },
  {
    '<leader>cn',
    function ()
      vim.cmd([[
        set background=dark
        colorscheme nord
      ]])
    end,
    desc = 'nord'
  },

  {
    '<leader>cv',
    function ()
      vim.cmd([[
        set background=dark
        colorscheme everforest
      ]])
    end,
    desc = 'everforest'
  },
  {
    '<leader>cx',
    toggle_bg,
    desc = 'toggle bg color'
  },
  {
    '<leader>cz',
    function ()
      vim.cmd('ColorizerToggle')
    end,
    desc = 'ColorizerToggle'
  },
}

return keymap

