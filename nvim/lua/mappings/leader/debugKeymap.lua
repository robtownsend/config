local dap = require('dap')
local dapui = require('dapui')
-- local uiVars = require('dap.ui.variables')
-- local uiWidgets = require('dap.ui.widgets')

local keymap = {
  name = "Debug",

  b = { function() dap.toggle_breakpoint() end, "set breakpoint" },

  s = {
    name = "Step",
    c = { function() dap.continue() end, "continue" },
    v = { function() dap.step_over() end, "step over" },
    i = { function() dap.step_into() end, "step into" },
    o = { function() dap.step_out() end, "step out" },
  },
  -- h = {
  --   name = "Hover",
  --   h = { function() uiVars.hover() end, "hover" },
  --   v = { function() uiVars.visual_hover() end, "visual hover" },
  -- },
  -- u = {
  --   name = "UI",
  --   h = { function() uiWidgets.hover() end, "hover" },
  --   f = { function() widgets.centered_float(uiWidgets.scopes) end, "float" },
  -- },
  u = {
    name = "UI",
    o = { function() dapui.open() end, "open" },
    c = { function() dapui.close() end, "close" },
  },

  v = { function() dap.scopes() end, "scopes" },
  t = { function() dap.toggle() end, "toggle" },

  r = {
    name = "Repl",
    o = { function() dap.repl.open() end, "open" },
    l = { function() dap.repl.run_last() end, "run last" },
  }
}

return keymap

