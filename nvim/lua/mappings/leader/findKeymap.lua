local keymap = {
  { '<leader>f', group = 'Find' },

  {
    '<leader>f:',
    function()
      vim.cmd('Telescope command_history')
    end,
    desc = 'cmd history'
  },
  {
    '<leader>fb',
    function()
      vim.cmd('Buffers')
    end,
    desc ='buffers'
  },

  { '<leader>fc', group = 'Commands' },
  {
    '<leader>fcc',
    function()
      vim.cmd('Telescope commands')
    end,
    desc = 'commands'
  },
  {
    '<leader>fch',
    function()
      vim.cmd('Telescope command_history')
    end,
    desc ='history'
  },

  {
    '<leader>ff',
    function()
      local telescope = require('telescope.builtin')
      telescope.find_files({ hidden = true })
    end,
    desc = 'files'
  },
  {
    '<leader>fg',
    function()
      vim.cmd('Telescope live_grep')
    end,
    desc = 'grep'
  },
  {
    '<leader>fl',
    function()
      vim.cmd('Telescope luasnip')
    end,
    desc = 'snippets'
  },
  {
    '<leader>fn',
    '<Cmd>Telescope file_browser path=%:p:h select_buffer=true<CR>',
    desc = 'file browser'
  },
  {
    '<leader>fr',
    function()
      vim.cmd('Telescope oldfiles')
    end,
    desc = 'recent files'
  },
  {
    '<leader>ft',
    function()
      vim.cmd('Telescope telescope-tabs list_tabs')
    end,
    desc = 'tabs'
  },
  {
    '<leader>fu',
    function()
      require("telescope").extensions.undo.undo()
    end,
    desc = 'undo tree'
  },
  {
    '<leader>fy',
    function()
      require("telescope").extensions.yank_history.yank_history()
    end,
    desc = 'yank history'
  }
}

return keymap
