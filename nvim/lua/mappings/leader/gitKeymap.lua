local keymap = {
  { '<leader>g', group = 'Git' },
  {
    '<leader>gb',
    function()
      vim.cmd('Telescope git_branches')
    end,
    desc = 'branches'
  },

  { '<leader>gc', group = 'Commits' },
  {
    '<leader>gcb',
    function()
      vim.cmd('Telescope git_bcommits')
    end,
    desc = 'buffer commits'
  },
  {
    '<leader>gcc',
    function()
      vim.cmd('Telescope git_commits')
    end,
    desc = 'repo commits'
  },

  { '<leader>gd', group = "Diff" },
  {
    '<leader>gdc',
    function()
      vim.cmd('tabclose')
    end,
    desc = 'close'
  },
  {
    '<leader>gdo',
    function()
      vim.cmd('DiffviewOpen')
    end,
    desc = 'open'
  },
  {
    '<leader>gdt',
    function()
      vim.cmd('DiffviewToggleFiles')
    end,
    desc = 'toggle file tree'
  },

  {
    '<leader>gm',
    function()
      vim.cmd('GitMessenger')
    end,
    desc = 'messenger'
  }
}

return keymap
