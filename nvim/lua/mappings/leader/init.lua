local flatten = require('utils.flatten')

local keymaps = {
  require('mappings.leader.alternates'),
  require('mappings.leader.buffersKeymap'),
  require('mappings.leader.colorsKeymap'),
  -- d = require('mappings.leader.debugKeymap'),
  require('mappings.leader.findKeymap'),
  require('mappings.leader.gitKeymap'),
  require('mappings.leader.lspKeymap'),
  {
    '<leader>n',
    function()
      vim.cmd('Telescope file_browser path=%:p:h select_buffer=true')
    end,
    desc = 'file browser'
  },
  require('mappings.leader.project'),
  {
    '<leader>q',
    function()
      vim.cmd('Sayonara!')
    end,
    desc = 'Sayonara!'
  },
  require('mappings.leader.shellKeymap'),
  require('mappings.leader.testKeymap'),
  require('mappings.leader.windowsKeymap'),
  {
    '<leader>y',
    function()
      vim.cmd('ZenMode')
    end,
    desc = 'ZenMode'
  },
  {
    '<leader>z',
    function()
      vim.cmd('e %')
    end,
    desc = 'Refresh buffer'
  }
}

local wk = require('which-key')
wk.add( flatten(keymaps) )
