local keymap = {
  { '<leader>l', group = 'Lsp' },
  {
    '<leader>lc',
    function()
      require("actions-preview").code_actions()
    end,
    desc = 'code actions'
  },
  {
    '<leader>ld',
    function()
      vim.diagnostic.open_float()
    end,
    desc = 'diag'
  },
  {
    '<leader>lf',
    function()
      vim.lsp.buf.format({ async = true })
    end,
    desc = 'format'
  },

  { '<leader>lg', group = 'Goto' },
  {
    '<leader>lgd',
    function()
      vim.cmd('Telescope lsp_definitions show_line=false')
    end,
    desc = 'definition'
  },
  {
    '<leader>lgi',
    function()
      vim.cmd('Telescope lsp_implementations')
    end,
    desc = 'implementations'
  },
  {
    '<leader>lgr',
    function()
      vim.cmd('Telescope lsp_references show_line=false')
    end,
    desc = 'references'
  },
  {
    '<leader>lgt',
    function()
      vim.cmd('Telescope lsp_type_definitions')
    end,
    desc = 'type definition'
  },

  {
    '<leader>lh',
    function()
      vim.lsp.buf.hover()
    end,
    desc = 'hover'
  },
  {
    '<leader>ll',
    function()
      local s = require("lsp_lines").toggle();
      print(string.format("lsp_lines %s", s and "on" or "off"))
    end,
    desc = 'lsp lines'
  },
  {
    '<leader>lr',
    function ()
      vim.lsp.buf.rename()
    end,
    desc = 'rename'
  },
  {
    '<leader>lw',
    function()
      vim.cmd('LspInfo')
    end,
    desc = 'LspInfo' },
  {
    '<leader>lz',
    function()
      vim.cmd('LspStart')
    end,
    desc = 'LspStart'
  },
}

return keymap
