local keymap = {
  { '<leader>p', group = 'Project' },
  {
    '<leader>pb',
    function()
      vim.cmd([[
        execute 'FloatermShow --name=build'
        execute 'FloatermSend --name=build yarn build'
      ]])
    end,
    desc = 'build'
  },
  {
    '<leader>pl',
    function()
      vim.cmd([[
        execute 'FloatermShow --name=lint'
        execute 'FloatermSend --name=lint yarn lint-check'
      ]])
    end,
    desc = 'lint'
  },
}

return keymap
