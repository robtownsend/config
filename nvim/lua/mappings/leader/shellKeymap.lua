local keymap = {
  { '<leader>s', group = "Shell" },
  {
    '<leader>sn',
    function()
      vim.cmd('FloatermNew --width=123 --height=30')
    end,
    desc = 'new shell'
  },
  {
    '<leader>ss',
    function()
      vim.cmd('Telescope floaterm')
    end,
    desc = 'list shells'
  },
  {
    '<leader>st',
    function()
      vim.cmd('FloatermToggle')
    end,
    desc = 'toggle'
  },
  {
    '<leader>sv',
    function()
      vim.cmd('FloatermToggle repoviewer')
    end,
    desc = 'repoviewer'
  },
}

return keymap

