local keymap = {
  { '<leader>t', group = 'Tests' },
  {
    '<leader>ta',
    function()
      vim.cmd('TestSuite')
    end,
    desc = 'all'
  },
  {
    '<leader>tf',
    function()
      vim.cmd('TestFile')
    end,
    desc = 'file'
  },
  {
    '<leader>tn',
    function()
      vim.cmd('TestNearest')
    end,
    desc = 'nearest'
  },
  {
    '<leader>tl',
    function()
      vim.cmd('TestLast')
    end,
    desc = 'last'
  },
  {
    '<leader>te',
    function()
      vim.cmd('split') -- ??
    end,
    desc = 'edit'
  },
}

return keymap

