local keymap = {
  { '<leader>w', group = 'Windows' },
  {
    '<leader>wd',
    function()
      vim.cmd('Sayonara')
    end,
    desc = 'delete'
  },
  {
    '<leader>wo',
    function()
      vim.cmd('wincmd o')
    end,
    desc = 'maximize'
  },
  {
    '<leader>ww',
    function()
      vim.cmd('Windows')
    end,
    desc = 'windows'
  },
}

return keymap

