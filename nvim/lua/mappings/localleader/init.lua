local file_browser_cmd = 'Telescope file_browser path=%:p:h select_buffer=true'

local keymap = {
  {
    '<localleader>b',
    function()
      vim.api.nvim_command('tabnew')
      require('telescope.builtin').buffers()
    end,
    desc = 'open buffer in new tab'
  },
  {
    '<localleader>f',
    function()
      vim.api.nvim_command('tabnew')
      require('telescope.builtin').find_files()
    end,
    desc = 'file picker in new tab'
  },
  {
    '<localleader>g',
    function()
      vim.api.nvim_command('tabnew')
      require('telescope.builtin').live_grep()
    end,
    desc = 'live grep in new tab'
  },

  { '<localleader>h', group = 'horizontal split' },
  {
    '<localleader>hb',
      function()
        vim.api.nvim_command('split')
        require('telescope.builtin').buffers()
      end,
      desc = 'buffers'
  },
  {
    '<localleader>hf',
    function()
      vim.api.nvim_command('split')
      require('telescope.builtin').find_files()
    end,
    desc = 'files'
  },
  {
    '<localleader>hn',
    function()
      vim.api.nvim_command('split')
      vim.api.nvim_command('Telescope file_browser path=%:p:h select_buffer=true')
    end,
    desc = 'nnn'
  },
  {
    '<localleader>r',
    function()
      local file = vim.fn.expand('%')
      vim.api.nvim_command('e ' .. file)
    end
  },

  {
    '<localleader>n',
    function()
      vim.api.nvim_command('tabnew')
      vim.api.nvim_command('Telescope file_browser path=%:p:h select_buffer=true')
    end,
    desc = 'nnn'
  },
{ '<localleader>v', group = 'vertical split' },
{
  '<localleader>vb',
    function()
      vim.api.nvim_command('vsplit')
      require('telescope.builtin').buffers()
    end,
    desc = 'buffers'
  },
  {
    '<localleader>vf',
    function()
      vim.api.nvim_command('vsplit')
      require('telescope.builtin').find_files()
    end,
    desc = 'files'
  },
  {
    '<localleader>vn',
    function()
      vim.api.nvim_command('vsplit')
      vim.api.nvim_command('Telescope file_browser path=%:p:h select_buffer=true')
    end,
    desc = 'nnn'
  },

  {
    '<localleader>w',
    function()
      local wrap = vim.o.wrap
      vim.o.wrap = not wrap
    end,
    desc = 'toggle wrap'
  }
}

local wk = require('which-key')
wk.add(keymap)
