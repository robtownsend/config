local keymap = {
  {
    '<C-M-w>h',
    function()
      vim.api.nvim_command('split')
    end,
    desc = 'horizontal split'
  },
  {
    '<C-M-w>x',
    function()
      vim.api.nvim_command('wincmd |')
    end,
    desc = 'maximize window'
  },
  {
    '<C-M-w>z',
    function()
      vim.api.nvim_command('WinShift')
    end,
    desc = 'move window'
  },
}

local wk = require('which-key')
wk.add(keymap)
-- Remapped to avoid colliding w/ browser close tab shortcut

