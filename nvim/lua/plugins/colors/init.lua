local m = {
  { 'NvChad/nvim-colorizer.lua', event = "VeryLazy", config = true },
  {
    'morhetz/gruvbox',
    lazy = false,
    config = function ()
      vim.cmd('colorscheme gruvbox')

      local hour = tonumber(os.date('%H'))
      if hour < 8 or hour >= 19 then
        vim.cmd('set background=dark')
      else
        vim.cmd('set background=light')
      end
    end
  },
  { 'icymind/neosolarized', lazy = false },
  { 'junegunn/seoul256.vim', lazy = false },
  { 'wolf-dog/nighted.vim', lazy = false },
  { 'sainnhe/everforest', lazy = false },
  { 'folke/tokyonight.nvim', lazy = false },
  { 'savq/melange', lazy = false },
  { 'arcticicestudio/nord-vim', lazy = false },
  { 'AlexvZyl/nordic.nvim', lazy = false }
}

return m
