local flatten = require('utils.flatten')

local plugins = {
  require('plugins.completions.nvim-cmp'),
  require('plugins.completions.luasnip'),

  { 'L3MON4D3/cmp-luasnip-choice' },
  { 'petertriho/cmp-git' },
  { 'hrsh7th/cmp-buffer' },
  { 'FelipeLema/cmp-async-path' },
  { 'hrsh7th/cmp-cmdline' },
  { 'dmitmel/cmp-cmdline-history' },
  { 'hrsh7th/cmp-nvim-lsp' },
  { 'hrsh7th/cmp-nvim-lsp-signature-help' },
  { 'saadparwaiz1/cmp_luasnip' },
  { 'L3MON4D3/cmp-luasnip-choice' },
  { 'hrsh7th/cmp-nvim-lua' },
  { 'windwp/nvim-autopairs' },
  { 'chrisgrieser/cmp_yanky' },
}

return flatten(plugins)
