local nvim_dir = '~/.config/nvim'
local snips_dir = nvim_dir .. '/lua/snippets'

return {
  'L3MON4D3/LuaSnip',
  version = "v2.*",
  event = "InsertEnter",
  build = "make install_jsregexp",
  config = function()
    require("luasnip").filetype_extend("typescript", { "javascript" })
    require("luasnip").filetype_extend("bash", { "sh" })
    require("luasnip").filetype_extend("zsh", { "sh" })

    require("luasnip.loaders.from_lua").lazy_load({paths = snips_dir})

    local ls = require('luasnip')

    vim.keymap.set(
      { 'i', 's' },
      '<C-k>',
      function()
        ls.expand()
      end,
      { silent = true }
    )

    vim.keymap.set(
      {"i", "s"},
      "<C-l>",
      function()
        ls.jump(1)
      end,
      {silent = true}
    )
    vim.keymap.set({"i", "s"}, "<C-j>", function() ls.jump(-1) end, {silent = true})

    vim.keymap.set({"i", "s"}, "<C-E>", function()
      if ls.choice_active() then
        ls.change_choice(1)
      end
    end, {silent = true})
  end
}
