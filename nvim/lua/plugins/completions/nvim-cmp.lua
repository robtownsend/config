return {
  'hrsh7th/nvim-cmp',
  event = { 'InsertEnter', 'CmdlineEnter' },
  dependencies = {
    'petertriho/cmp-git',
    'hrsh7th/cmp-buffer',
    'FelipeLema/cmp-async-path',
    'hrsh7th/cmp-cmdline',
    'dmitmel/cmp-cmdline-history',
    'octaltree/cmp-look',

    -- lsp
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-nvim-lsp-signature-help',

    -- snippets
    'L3MON4D3/LuaSnip',
    'saadparwaiz1/cmp_luasnip',
    'L3MON4D3/cmp-luasnip-choice',

    -- langs
    'hrsh7th/cmp-nvim-lua',

    --misc
    'windwp/nvim-autopairs',
    'chrisgrieser/cmp_yanky',
  },
  config = function()
    require("conf.completions")
  end,
}
