return {
  'kazhala/close-buffers.nvim',
  event = "VeryLazy",
  config = {
    preserve_window_layout = { 'this', 'nameless' }
  }
}
