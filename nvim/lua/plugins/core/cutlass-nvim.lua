return {
  'gbprod/cutlass.nvim',
  event = "VeryLazy",
  config = function ()
    require("cutlass").setup({
      registers = {
        delete = "d",
        change = "c"
      },
      exclude = { "ns", "nS" }
    })
  end
}
