return {
  "folke/flash.nvim",
  event = "VeryLazy",
  -- cond = function()
  --   return not ( vim.bo.filetype == 'text' or vim.bo.filetype == 'markdown' )
  -- end,
  opts = {},
  keys = {
    {
      "j",
      mode = { "n", "o", "x" },
      function()
        require("flash").jump({
          search = {
            -- Match beginning of words only
            mode = function(str)
              return "\\<" .. str
            end,
          },
        })
      end,
      desc = "Flash"
    },
    {
      "J",
      mode = { "n", "o", "x" },
      function()
        require("flash").treesitter()
      end,
      desc = "Flash Treesitter"
    },
    {
      "r",
      mode = "o",
      function()
        require("flash").remote()
      end,
      desc = "Remote Flash" },
    {
      "R", mode = { "o", "x" }, function() require("flash").treesitter_search() end, desc = "Treesitter Search" },
    { "<c-s>", mode = { "c" }, function() require("flash").toggle() end, desc = "Toggle Flash Search" },
  },
}
