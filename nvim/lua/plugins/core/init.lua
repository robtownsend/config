local flatten = require('utils.flatten')

local plugins = {
  require('plugins.core.auto-save-nvim'),
  require('plugins.core.vim-numbertoggle'),
  require('plugins.core.flash-nvim'),
  require('plugins.core.cutlass-nvim'),
  require('plugins.core.close-buffers-nvim'),
  require('plugins.core.substitute-nvim'),
  require('plugins.core.yanky-nvim'),
  require('plugins.core.nvim-pqf'),
  require('plugins.core.winshift-nvim'),
  require('plugins.core.inc-rename-nvim'),
  require('plugins.core.fine-cmdline-nvim'),
  require('plugins.core.which-key-nvim'),

  { 'mhinz/vim-sayonara', cmd = "Sayonara" },
  { 'editorconfig/editorconfig-vim', event = "BufReadPre" },
  { 'tpope/vim-repeat', event = "VeryLazy" },
  { 'chentoast/marks.nvim', config = true, event = "VeryLazy" },
  { 'tpope/vim-abolish', event = 'BufReadPost' },
}

return flatten(plugins)
