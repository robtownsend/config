return {
  -- Prettier quickfix / location list
  url = 'https://gitlab.com/yorickpeterse/nvim-pqf.git',
  event = "QuickFixCmdPre",
  config = true,
}
