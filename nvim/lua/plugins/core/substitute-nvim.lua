return {
  'gbprod/substitute.nvim',
  event = "VeryLazy",
  config = function()
    require("substitute").setup({
      on_substitute = require("yanky.integration").substitute(),
    })

    vim.keymap.set("n", "x", require('substitute').operator, { noremap = true })
    vim.keymap.set("x", "x", require('substitute').visual, { noremap = true })
  end
}
