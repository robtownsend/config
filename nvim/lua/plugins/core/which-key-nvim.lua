return {
  {
    'folke/which-key.nvim',
    event = "VeryLazy",
    config = function()
      -- By default timeoutlen is 1000 ms
      vim.opt.timeoutlen = 750

      require("mappings")
    end
  },
}
