return {
  'gbprod/yanky.nvim',
  event = "VeryLazy",
  config = function()
    require('yanky').setup({
      ring = {
        history_length = 1000
      },
      highlight = {
        on_put = true,
        on_yank = false,
        timer = 250,
      },
    })

    vim.keymap.set({"n","x"}, "p", "<Plug>(YankyPutAfter)")
    vim.keymap.set({"n","x"}, "P", "<Plug>(YankyPutBefore)")
    vim.keymap.set({"n","x"}, "gp", "<Plug>(YankyGPutAfter)")
    vim.keymap.set({"n","x"}, "gP", "<Plug>(YankyGPutBefore)")

    -- Yanky comes with special put moves (inspired by tpope/vim-unimpaired):
    --
    -- Linewise put: this will force put above or below the current line ;
    -- Shift right/left put: will put above or below the current line and increasing or decreasing indent ;
    -- Filter put: will put above or below the current line and reindenting.
    vim.keymap.set("n", "]p", "<Plug>(YankyPutIndentAfterLinewise)")
    vim.keymap.set("n", "[p", "<Plug>(YankyPutIndentBeforeLinewise)")
    vim.keymap.set("n", "]P", "<Plug>(YankyPutIndentAfterLinewise)")
    vim.keymap.set("n", "[P", "<Plug>(YankyPutIndentBeforeLinewise)")

    vim.keymap.set("n", ">p", "<Plug>(YankyPutIndentAfterShiftRight)")
    vim.keymap.set("n", "<p", "<Plug>(YankyPutIndentAfterShiftLeft)")
    vim.keymap.set("n", ">P", "<Plug>(YankyPutIndentBeforeShiftRight)")
    vim.keymap.set("n", "<P", "<Plug>(YankyPutIndentBeforeShiftLeft)")

    vim.keymap.set("n", "=p", "<Plug>(YankyPutAfterFilter)")
    vim.keymap.set("n", "=P", "<Plug>(YankyPutBeforeFilter)")
  end
}
