local m = {
  {
    'mfussenegger/nvim-dap',
    name = 'nvim-dap',
    dependencies = {
      'nvim-neotest/nvim-nio'
    },
    config = function()
      require("conf.debugging.dap-config")
    end
  },
  {
    'rcarriga/nvim-dap-ui',
    dependencies = {
      'nvim-dap',
      { 'theHamsta/nvim-dap-virtual-text', config = true },
      'mason-nvim-dap',
      'jbyuki/one-small-step-for-vimkind'
    },
    config = function()
      require("conf.debugging.dap-ui-config")
    end
  },
  {
    'jay-babu/mason-nvim-dap.nvim',
    name = 'mason-nvim-dap',
    dependencies = { 'mason' },
    config = function()
      require("mason-nvim-dap").setup({
        ensure_installed = { "node-debug2-adapter" },
        automatic_installation = true,
        automatic_setup = true
      })
    end
  }
}

return m
