return {
  'lewis6991/gitsigns.nvim',
  event = "BufReadPost",
  config = true,
  dependencies = {
    "nvim-lua/plenary.nvim"
  },
}
