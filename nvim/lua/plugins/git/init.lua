local flatten = require('utils.flatten')

local plugins = {
  require('plugins.git.gitsigns'),
  require('plugins.git.git-messenger-vim'),
  require('plugins.git.git-blame-nvim'),
  require('plugins.git.diffview-nvim'),
  require('plugins.git.tardis-nvim'),
}

return flatten(plugins)
