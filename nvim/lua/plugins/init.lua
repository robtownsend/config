local flatten = require('utils.flatten')

-- bootstrap lazy plugin manager
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.runtimepath:prepend(lazypath)

local plugins = {
  require('plugins.core'),
  require('plugins.colors'),
  require('plugins.treesitter'),
  require('plugins.lsp'),
  require('plugins.project_management'),
  require('plugins.utils'),
  require('plugins.debugging'),
  require('plugins.languages'),
  require('plugins.testing'),
  require('plugins.git'),
  require('plugins.completions'),
  require('plugins.misc'),
}

require("lazy").setup({
  flatten(plugins)
})

