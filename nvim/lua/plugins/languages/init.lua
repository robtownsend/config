local flatten = require('utils.flatten')

local m = {
  require('plugins.languages.javascript'),
  require('plugins.languages.lua'),
  require('plugins.languages.markdown'),
  require('plugins.languages.terraform')
}

return flatten(m)
