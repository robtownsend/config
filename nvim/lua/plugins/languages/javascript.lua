local plugins = {
  {
    'psychollama/further.vim', -- Use gf to follow thru node imports
    ft = {
      "javascript",
      "typescript",
      "typescriptreact", -- tsx
    }
  },
  { 'yuezk/vim-js', ft = "javascript" },
}

return plugins
