local plugins = {
  { 'sam4llis/nvim-lua-gf', ft = 'lua' },
}

return plugins
