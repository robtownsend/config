local plugins = {
  {
    'preservim/vim-textobj-sentence',
    ft = { 'text', 'markdown' },
  },
  {
    'preservim/vim-textobj-quote',
    ft = { 'text', 'markdown' },
  },
}

return plugins
