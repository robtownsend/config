local plugins = {
  { 'hashivim/vim-terraform', ft = "terraform" },
}

return plugins
