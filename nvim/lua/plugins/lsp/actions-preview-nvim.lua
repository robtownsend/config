return {
  'aznhe21/actions-preview.nvim',
  event = 'VeryLazy',
  dependencies = {
    'nvim-lspconfig',
    'MunifTanjim/nui.nvim',
    'nvim-telescope/telescope.nvim'
  },
  config = function()
    require('actions-preview').setup({
      telescope = {
        layout_strategy = "vertical",
        layout_config = {
          width = 0.9,
          height = 0.9,
          preview_cutoff = 1,
          mirror = true,
          prompt_position = "top",
        },
      }
    })
  end,
}
