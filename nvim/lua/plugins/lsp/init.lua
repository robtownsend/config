local flatten = require('utils.flatten')

local plugins = {
-- Loading must occur in order of mason -> mason-lspconfig -> nvim-lspconfig
  require('plugins.lsp.nvim-lspconfig'),
  require('plugins.lsp.mason-nvim'),
  require('plugins.lsp.mason-lspconfig-nvim'),
  require('plugins.lsp.none-ls-nvim'),
  require('plugins.lsp.nvim-lightbulb'),
  require('plugins.lsp.actions-preview-nvim'),
  require('plugins.lsp.lsp-lines-nvim'),
}

return flatten(plugins)
