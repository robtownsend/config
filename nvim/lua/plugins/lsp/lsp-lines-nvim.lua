return {
  name = "lsp_lines",
  url = 'https://git.sr.ht/~whynothugo/lsp_lines.nvim',
  dependencies = { "nvim-lspconfig" },
  event = "VeryLazy",
  config = function()
    -- Disable virtual text for lsp diagnostics
    vim.diagnostic.config({
      virtual_text = false,
    })

    require("lsp_lines").setup()
    -- switch off initially
    vim.diagnostic.config({ virtual_lines = false })
  end
}
