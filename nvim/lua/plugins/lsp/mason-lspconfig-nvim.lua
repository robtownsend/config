return {
    'williamboman/mason-lspconfig.nvim',
    name = 'mason-lspconfig',
    dependencies = { 'mason' },
    config = function()
      require('conf.lsp.mason-lspconfig')
    end
}
