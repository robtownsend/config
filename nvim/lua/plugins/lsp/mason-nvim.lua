return {
  "williamboman/mason.nvim",
  name = "mason",
  config = function()
    require("conf.lsp.mason")
  end
}
