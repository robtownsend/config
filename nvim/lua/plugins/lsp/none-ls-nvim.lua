return {
  'nvimtools/none-ls.nvim',
  event = 'VeryLazy',
  dependencies = { 'neovim/nvim-lspconfig' },
  config = function()
    local null_ls = require('null-ls')
    local formatters = null_ls.builtins.formatting
    local diagnostics = null_ls.builtins.diagnostics
    local code_actions = null_ls.builtins.code_actions

    null_ls.setup({
      sources = {
        formatters.prettier,
        formatters.stylua,
        formatters.shfmt,
        diagnostics.markdownlint,
      },
    })
  end
}
