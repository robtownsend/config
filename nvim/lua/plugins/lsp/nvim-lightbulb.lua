return {
  'kosayoda/nvim-lightbulb',
  event = 'VeryLazy',
  dependencies = { 'neovim/nvim-lspconfig' },
  config = function()
    require('nvim-lightbulb').setup({
      hide_in_unfocused_buffer = false,
      autocmd = { enabled = true },
      virtual_text = { enabled = true }
    })
  end
}
