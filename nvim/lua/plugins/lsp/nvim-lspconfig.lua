return {
  'neovim/nvim-lspconfig',
  name = 'nvim-lspconfig',
  event = 'VeryLazy',
  dependencies = {
    "mason-lspconfig",
    -- "RRethy/vim-illuminate"
  }
}
