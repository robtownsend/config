return {
  "intrntbrn/awesomewm-vim-tmux-navigator",
  event = "VeryLazy",
  build = "git -C ~/.config/awesome/awesomewm-vim-tmux-navigator/ pull",
  init = function()
    vim.g.tmux_navigator_no_mappings = 1
    -- vim.g.tmux_navigator_no_dynamic_title = 1
    -- vim.g.tmux_navigator_save_on_switch = 1
    -- vim.g.tmux_navigator_disable_when_zoomed = 1
    -- vim.g.tmux_navigator_preserve_zoom = 1

    vim.keymap.set("n", "<C-h>", ":TmuxNavigateLeft<CR>", { noremap = true, silent = true })
    vim.keymap.set("n", "<C-j>", ":TmuxNavigateDown<CR>", { noremap = true, silent = true })
    vim.keymap.set("n", "<C-k>", ":TmuxNavigateUp<CR>", { noremap = true, silent = true })
    vim.keymap.set("n", "<C-l>", ":TmuxNavigateRight<CR>", { noremap = true, silent = true })
  end
}
