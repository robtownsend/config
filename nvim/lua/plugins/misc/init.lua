local flatten = require('utils.flatten')

local plugins = {
  require('plugins.misc.lualine'),
  require('plugins.misc.alpha-nvim'),
  require('plugins.misc.awesomewm-vim-tmux-navigator'),
  require('plugins.misc.nvim-startup'),
  require('plugins.misc.pigeon'),
  require('plugins.misc.zen-mode-nvim'),
  { 'MunifTanjim/nui.nvim' },
  { 'ryanoasis/vim-devicons' },
  { 'kyazdani42/nvim-web-devicons' },
  { 'echasnovski/mini.icons' },
  {
    --Add colors to charts
    'powerman/vim-plugin-AnsiEsc',
    event = "VeryLazy"
  },
}

return flatten(plugins)
