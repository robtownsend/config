return {
  'nvim-lualine/lualine.nvim',
  lazy = false,
  config = {
    sections = {
      lualine_a = {'mode'},
      lualine_b = {'branch', 'diff', 'diagnostics'},
      lualine_c = {'filename'},
      lualine_g = {'buffers'},
      lualine_h = {'windows'},
      lualine_x = {'filetype'},
      lualine_y = {'progress'},
      lualine_z = {'location'}
    }
  }
}
