return {
  url = 'https://git.sr.ht/~henriquehbr/nvim-startup.lua',
  name = 'nvim-startup',
  lazy = false,
  config = function()
    require('nvim-startup').setup({
      startup_file = '/tmp/nvim-startuptime',
      message = function(time)
        -- For use by alpha dashboard
        vim.g.startuptime = time

        return ''
      end
    })
  end
}
