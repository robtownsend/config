return {
  "Pheon-Dev/pigeon",
  -- event = "",
  config = function()
    require("pigeon").setup({
      enabled = true,
      os = "linux",
      plugin_manager = "lazy",
      callbacks = {
        killing_pigeon = nil,
        respawning_pigeon = nil,
      },
      -- more config options here
    })
  end
}
