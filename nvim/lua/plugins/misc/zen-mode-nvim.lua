return {
  'folke/zen-mode.nvim',
  lazy = false,
  opts = { window = {width = 90} }
}
