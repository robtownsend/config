local flatten = require('utils.flatten')

local plugins = {
  require('plugins.project_management.dotenv-nvim'),
  require('plugins.project_management.neorg'),
  { 'tpope/vim-projectionist', lazy = false },
  { 'tpope/vim-dispatch', cmd = { "Dispatch", "Make" } },
}

return flatten(plugins)
