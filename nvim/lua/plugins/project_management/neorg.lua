local writings_dir = os.getenv('WRITINGS_DIR')

return {
  "nvim-neorg/neorg",
  filetype = { 'norg' },
  lazy = false,
  -- cmd = 'Neorg',
  version = "*", -- Pin to latest stable release
  config = {
    load = {
      ["core.defaults"] = {},
      ["core.concealer"] = {},
      ["core.completion"] = {
        config = {
          engine = "nvim-cmp"
        }
      },
      ["core.journal"] = {
        config = {
          workspace = 'journal',
          journal_folder = '',
        },
      },
      ["core.dirman"] = {
        config = {
          workspaces = {
            experiments = writings_dir .. "/experiments",
            notes = writings_dir .. "/notes",
            journal = writings_dir .. "/journal",
          },
          default_workspace = 'notes',
        },
      },
    }
  },
}
