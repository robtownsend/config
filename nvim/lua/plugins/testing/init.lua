local m = {
  {
    'vim-test/vim-test',
    cmd = {
      "TestSuite",
      "TestFile",
      "TestLast",
      "TestNearest",
    },
    dependencies = { 'voldikss/vim-floaterm' },
    config = function()
      vim.cmd([[ 
        function! FloatermPersistStrategy(cmd)
          execute 'FloatermShow testRunner'
          execute 'FloatermSend --name=testRunner ' . a:cmd
        endfunction

        let g:test#custom_strategies = {'floatermPersist': function('FloatermPersistStrategy')}
        let g:test#strategy = 'floatermPersist'

        " Don't echo the test command before running it
        let g:test#echo_command = 0
      ]])
    end
  }
}

return m
