local flatten = require('utils.flatten')

local highlight = {
  "RainbowRed",
  "RainbowYellow",
  "RainbowBlue",
  "RainbowOrange",
  "RainbowGreen",
  "RainbowViolet",
  "RainbowCyan",
}

local plugins = {
  require('plugins.treesitter.nvim-treesitter'),
  require('plugins.treesitter.rainbow-delimiters-nvim')(highlight),
  require('plugins.treesitter.indent-blankline-nvim')(highlight),
  require('plugins.treesitter.nvim-surround'),
  require('plugins.treesitter.vim-matchup'),
  require('plugins.treesitter.nvim-autopairs'),
  require('plugins.treesitter.comment-nvim'),
}

return flatten(plugins)
