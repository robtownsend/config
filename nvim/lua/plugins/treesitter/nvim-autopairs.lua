return {
  'windwp/nvim-autopairs',
  event = "InsertEnter",
  config = function()
    require('nvim-autopairs').setup({
      check_ts = true,
      ignored_next_char = "[%w%.]" -- will ignore alphanumeric and `.` symbol
    })
  end
}
