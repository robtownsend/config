return {
  "nvim-treesitter/nvim-treesitter",
  dependencies = {
    { "nvim-treesitter/playground", cmd = "TSPlaygroundToggle" },
    'JoosepAlviste/nvim-ts-context-commentstring',
    "nvim-treesitter/nvim-treesitter-textobjects",
    "RRethy/nvim-treesitter-textsubjects",
    "windwp/nvim-ts-autotag",
    'andymass/vim-matchup',
  },
  build = ":TSUpdate",
  event = "VeryLazy",
  config = function()
    require("conf.treesitter")
  end,
}

