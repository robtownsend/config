return {
  'andymass/vim-matchup',
  event = "VeryLazy",
  config = function()
    vim.g.matchup_matchparen_offscreen = { method = 'popup'}
    vim.g.matchup_text_obj_linewise_operators = { 'c', 'd', 'y' }

    -- Enables dynamically changing a matching tag when editing a delim
    -- i.e. given <div></div>,changing the text of the opening div
    -- will auto change the closing tag to match
    vim.g.matchup_transmute_enabled = 1

    -- enables delete surrounding (ds%) and change surrounding (cs%) maps
    vim.g.matchup_surround_enabled = 1
  end
}
