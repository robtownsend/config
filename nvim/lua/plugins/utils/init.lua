local flatten = require('utils.flatten')

local plugins = {
  require('plugins.utils.telescope-nvim'),
  -- { 'nvim-telescope/telescope-fzf-native.nvim' },
  require('plugins.utils.vim-floaterm'),
}

return flatten(plugins)
