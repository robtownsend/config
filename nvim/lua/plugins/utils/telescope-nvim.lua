return {
  'nvim-telescope/telescope.nvim',
  tag = '0.1.4',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'nvim-telescope/telescope-fzf-native.nvim',
    'gbprod/yanky.nvim',
    -- extensions
    'debugloop/telescope-undo.nvim',
    'LukasPietzschmann/telescope-tabs',
    'dawsers/telescope-floaterm.nvim',
    'benfowler/telescope-luasnip.nvim',
    'nvim-telescope/telescope-file-browser.nvim',
  },
  cmd = 'Telescope',
  config = function ()
    local telescope = require('telescope')

    telescope.setup({
      dynamic_preview_title = true,
      defaults = {
        layout_strategy = "vertical",
        layout_config = {
          width = 0.9,
          height = 0.9,
          preview_cutoff = 1,
          mirror = true,
          prompt_position = "top",
        },
      },
      pickers = {
        find_files = {
          find_command = {
            'rg', '--files', '--hidden', '-g', '!.git'
          }
        },
        oldfiles = { cwd_only = true },
        colorscheme = { enable_preview = true },
      },
      extensions = {
        file_browser = {
          theme = 'ivy',
          layout_config = { mirror = false }
        }
      }
    })

    telescope.load_extension('undo')
    telescope.load_extension('yank_history')
    telescope.load_extension('floaterm')
    telescope.load_extension('luasnip')
    telescope.load_extension('file_browser')
  end
}
