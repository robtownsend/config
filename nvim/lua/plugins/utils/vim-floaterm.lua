return {
  'voldikss/vim-floaterm',
  lazy = false,
  config = function()
    vim.g.floaterm_autoclose = 0
    vim.g.floaterm_width = 0.9
    vim.g.floaterm_height = 0.9

    vim.cmd([[
      execute 'FloatermNew --name=testRunner --title=testRunner --silent'
      execute 'FloatermNew --name=build --title=build --silent'
    ]])
  end
}
