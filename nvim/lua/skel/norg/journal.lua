local fn = require('luasnip').function_node
local i = require('luasnip').insert_node
local fmt = require("luasnip.extras.fmt").fmt

local function get_date_created()
  local filename = vim.fn.expand("%:t")
  local today = os.time()
  local yest = os.time() - 60 * 60 * 24 -- 24 hrs

  local offset

  -- Sep_19_2024
  if string.find(filename, os.date("%b %d %Y")) then
    offset = today
  else
    offset = yest
  end

  return os.date("%A %B %d %Y", offset)
end

return fmt(
  [[
    ---
    date: {}
    daily_log:
      - {}
    ---
  ]],
  { fn(get_date_created), i(0) }
)
