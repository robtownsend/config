local fn = require('luasnip').function_node
local fmta = require("luasnip.extras.fmt").fmta

local function get_date_created()
  return os.date("%A %B %d %Y")
end

return fmta(
  [[
    ---
    date: <> 
    ---
  ]],
  { fn(get_date_created) }
)
