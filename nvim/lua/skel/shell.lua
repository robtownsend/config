local fn = require('luasnip').function_node
local fmta = require("luasnip.extras.fmt").fmta
local get_filename = require('utils.get_filename')

local function filename()
  return vim.fn.fnamemodify(
    get_filename(),
    ':r'
  )
end

return fmta(
  [[
    #!/usr/bin/env bash
    set -e

    utils_dir="$DOTS_DIR/lib/utils"
    for f in "$utils_dir"/*.sh; do
      # shellcheck source=/dev/null
      source "$f"
    done

    <>() {

    }
  ]],
  { fn(filename) }
)
