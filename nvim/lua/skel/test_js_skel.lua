local ls = require("luasnip")
local fmta = require("luasnip.extras.fmt").fmta
local fn = ls.function_node
local capitalize = require('utils.capitalize')
local get_filename = require('utils.get_filename')
local read_cmd_output = require('utils.read_cmd_output')

local function subject()
  local filename = vim.fn.fnamemodify(
    get_filename(),
    ':r' -- Remove .test extension
  )

  return capitalize(filename)
end

local function alternate()
  local test_file_dir = vim.fn.fnamemodify(
    vim.fn.expand('%'),
    ':p:h'
  )
  local src_file_dir = vim.fn.fnamemodify(
    vim.fn.expand('#'),
    ':p:h'
  )
  local cmd = string.format("\z
    realpath \z
      --relative-to=%s \z
      %s \z
    ",
    test_file_dir,
    src_file_dir
  )
  local rel_path = (read_cmd_output(cmd)):gsub("%s+", "")

  return rel_path .. '/' .. vim.fn.fnamemodify(
    vim.fn.expand('#'),
    ':t'
  )
end

return fmta(
  [[
    import { expect } from 'chai'
    import { } from '<>'
    
    describe('<>', function() {
      it('', function() {

      })
    })
  ]],
  {
    fn(alternate),
    fn(subject)
  }
)
