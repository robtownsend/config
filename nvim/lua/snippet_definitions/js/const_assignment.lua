local ls = require("luasnip")
local i = ls.insert_node
local fmt = require("luasnip.extras.fmt").fmt

return fmt([[
    const {} = {}
  ]],
  { i(1), i(2) }
)
