local ls = require("luasnip")
local i = ls.insert_node
local fmta = require("luasnip.extras.fmt").fmta

return fmta(
  [[
    const { <fields> } = <var>
  ]],
  { var = i(1), fields = i(2) }
)
