local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local c = ls.choice_node
local sn = ls.snippet_node
local fmt = require("luasnip.extras.fmt").fmt

local import_choices = c(2, {
  sn(1, {
    t('{ '),
    i(1),
    t(' } ')
  }),
  i(2)
})

return fmt([[
    import {imports} from '{lib}'
  ]],
  { imports = import_choices, lib = i(1) }
)
