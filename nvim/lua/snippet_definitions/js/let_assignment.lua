local ls = require("luasnip")
local i = ls.insert_node
local fmt = require("luasnip.extras.fmt").fmt

return fmt(
  [[
    let {} = {}   
  ]],
  { i(1), i(2) }
)
-- return {
--   t('let '),
--   i(1),
--   t(' = '),
--   i(2)
-- }
