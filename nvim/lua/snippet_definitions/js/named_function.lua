local ls = require("luasnip")
local i = ls.insert_node
local fmta = require("luasnip.extras.fmt").fmta

return fmta(
  [[
    function <>(<>) {
      <>
    }
  ]],
  { i(1), i(2), i(3) }
)
