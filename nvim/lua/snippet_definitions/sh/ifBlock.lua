local ls = require("luasnip")
local i = ls.insert_node
local fmt = require("luasnip.extras.fmt").fmt

return fmt([[
    if {}; then
      
    fi
  ]],
  { i(0) }
)
