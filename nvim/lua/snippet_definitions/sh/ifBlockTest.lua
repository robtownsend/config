local ls = require("luasnip")
local i = ls.insert_node
local t = ls.text_node
local fmt = require("luasnip.extras.fmt").fmt

-- Need to do this trick with the text node to avoid closing the string early
local template = [[
  if [[ {} {}; then
    {}
  fi
]]

return fmt(
  template,
  { i(1), t(']]'), i(0) }
)
