local ls = require("luasnip")
local i = ls.insert_node
local fmta = require("luasnip.extras.fmt").fmta
local get_filename = require('utils.get_filename')

return fmta(
  [[
    import React from 'react'

    interface Props {

    }

    export default function <name>(props: Props) {
      <body>

      return <retVal>
    }
  ]],
  {
    name = i(1, get_filename()),
    body = i(2),
    retVal = i(3)
  }
)
