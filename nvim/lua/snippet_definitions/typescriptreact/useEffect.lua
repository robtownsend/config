local ls = require("luasnip")
local i = ls.insert_node
local fmta = require("luasnip.extras.fmt").fmta
local l = require("luasnip.extras").lambda
local capitalize = require('utils.capitalize')

return fmta(
  [[
    useEffect(() => {
      <>
    }, [<>])
  ]],
  { i(1), i(2) }
)
