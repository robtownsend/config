local ls = require("luasnip")
local i = ls.insert_node
local c = ls.choice_node
local t = ls.text_node
local fmt = require("luasnip.extras.fmt").fmt

local refTypeChoices = {
  fmt('<{}>', { i(1) }),
  t('')
}

return fmt(
  [[
    const {name}Ref = useRef{type}({val})
  ]],
  {
    name = i(1),
    val = i(2),
    type = c(3, refTypeChoices),
  }
)
