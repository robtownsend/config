local ls = require("luasnip")
local i = ls.insert_node
local c = ls.choice_node
local t = ls.text_node
local fmt = require("luasnip.extras.fmt").fmt
local l = require("luasnip.extras").lambda
local capitalize = require('utils.capitalize')

return fmt(
  [[
    const [{name}, {setter}] = useState{type}({val})
  ]],
  {
    name = i(1),
    setter = l('set' .. capitalize(l._1), 1),
    val = i(2),
    type = c(3, {
      t(''),
      fmt('<{}>', i(1))
    }),
  }
)
