local ls = require("luasnip")
local s = ls.snippet

local console_log = require('snippet_definitions.js.console_log')
local console_log_var = require('snippet_definitions.js.console_log_var')
local import = require('snippet_definitions.js.import')
local const_assignment = require('snippet_definitions.js.const_assignment')
local let_assignment = require('snippet_definitions.js.let_assignment')
local destructuring = require('snippet_definitions.js.destructuring')
local destructure_multi_line = require('snippet_definitions.js.destructure_multi_line')
local anon_func = require('snippet_definitions.js.anon_func')
local named_func = require('snippet_definitions.js.named_function')
local iife = require('snippet_definitions.js.iife')
local arrow_func = require('snippet_definitions.js.arrow_func')
local arrow_func_parens = require('snippet_definitions.js.arrow_func_parens')
-- local arrow_func_block = require('.snippet_definitions.js.arrow_func_block')
local arrow_func_block_parens = require('snippet_definitions.js.arrow_func_block_parens')
local interface_define = require('snippet_definitions.js.interface_define')
-- local = require('.snippet_definitions.js.')
-- local = require('.snippet_definitions.js.')
-- local = require('.snippet_definitions.js.')
-- local = require('.snippet_definitions.js.')

-- local sn = ls.snippet_node
-- local isn = ls.indent_snippet_node
-- local t = ls.text_node
-- local i = ls.insert_node
-- local f = ls.function_node
-- local c = ls.choice_node
-- local d = ls.dynamic_node
-- local r = ls.restore_node
-- local events = require("luasnip.util.events")
-- local ai = require("luasnip.nodes.absolute_indexer")
-- local extras = require("luasnip.extras")
-- local l = extras.lambda
-- local rep = extras.rep
-- local p = extras.partial
-- local m = extras.match
-- local n = extras.nonempty
-- local dl = extras.dynamic_lambda
-- local fmt = require("luasnip.extras.fmt").fmt
-- local fmta = require("luasnip.extras.fmt").fmta
-- local conds = require("luasnip.extras.expand_conditions")
-- local postfix = require("luasnip.extras.postfix").postfix
-- local types = require("luasnip.util.types")
-- local parse = require("luasnip.util.parser").parse_snippet
-- local ms = ls.multi_snippet
-- local k = require("luasnip.nodes.key_indexer").new_key

local snips = {
  -- console.log('$1')$0
  s('cl', console_log),

  -- console.log('$1', $2)$0
  s('cll', console_log_var),

  -- import { c(2, { $c1 } || $c2) } from '$1'$0
  s('imp', import),

  -- const $1 = $2
  s('c', const_assignment),

  -- let $1 = $2
  s('l', let_assignment),

  -- const { $1 } = $2
  s('ds', destructuring),
  
  -- const {
  --   $2
  -- } = $1
  s('dss', destructure_multi_line),

  -- function($1) {
  --   $2
  -- }
  s('fn', anon_func),

  -- function $1($2) {
  --   $3
  -- }
  s('fnn', named_func),

  -- $1 => $2
  s('af', arrow_func),

  -- ($1) => $2
  s('afp', arrow_func_parens),

  -- $1 => {
  --   $2
  -- }
  -- this one breaks luasnip somehow
  -- s('afb', arrow_func_block),

  s('afbp', arrow_func_block_parens),

  -- (function() {
  --
  -- }())
  s('iife', iife),

  -- interface $1 {
  --   $2
  -- }
  s('int', interface_define),
}

local autosnips = {}

return snips, autosnips
