local ls = require("luasnip")
local s = ls.snippet

local frontmatter = require('snippet_definitions.norg.frontmatter')

local snips = {
  s('fm', frontmatter),
}

local autosnips = {}

return snips, autosnips
