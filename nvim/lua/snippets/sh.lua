local ls = require("luasnip")
local s = ls.snippet

local ifBlock = require('snippet_definitions.sh.ifBlock')
local ifBlockTest = require('snippet_definitions.sh.ifBlockTest')
local func = require('snippet_definitions.sh.func')
local forLoop = require('snippet_definitions.sh.forLoop')
local arrayLength = require('snippet_definitions.sh.arrayLength')

local snips = {
  s('ifb', ifBlock),
  s('ift', ifBlockTest),
  s('fn', func),
  s('fl', forLoop),
  s('arr_len', arrayLength),
}

local autosnips = {}

return snips, autosnips
