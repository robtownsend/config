local ls = require("luasnip")
local s = ls.snippet

local hookSkel = require('snippet_definitions.typescriptreact.hookSkel')
local useState = require('snippet_definitions.typescriptreact.useState')
local useRef = require('snippet_definitions.typescriptreact.useRef')
-- local useEffect = require('snippet_definitions.typescriptreact.useEffect')

local snips = {
  s('hook', hookSkel),
  s('ust', useState),
  s('ref', useRef),
  -- s('usf', useEffect),
}

local autosnips = {}

return snips, autosnips
