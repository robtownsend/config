local function flatten(nestedArr)
  local flatArr = {}

  for _,el in pairs(nestedArr) do
    table.insert(flatArr, el)
  end

  return flatArr
end

return flatten
