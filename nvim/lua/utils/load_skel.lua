return function (pattern, skel_file)
  vim.api.nvim_create_autocmd(
    { "BufEnter" },
    {
      pattern = pattern,
      callback = function()
        local ls = require('luasnip')
        local s = ls.snippet
        local snippet_def = require(skel_file)
        local buf = vim.api.nvim_get_current_buf()

        local lines = vim.api.nvim_buf_line_count(buf)
        if (lines ~= 1) then return end

        local line = vim.api.nvim_buf_get_lines(buf, 0, 1, false)[1]
        if (line ~= "") then return end

        ls.snip_expand(s('', snippet_def))
        vim.api.nvim_feedkeys(
          vim.api.nvim_replace_termcodes("<Esc>gg^:w<CR>", true, true, true),
          "n",
          true
        )
      end
    }
  )
end
