return function(cmd)
  local handle = io.popen(cmd)
  if (handle == nil) then
    os.execute('notify-send "failed to open handle"')

    return ""
  end

  local result = handle:read("*a")
  handle:close()

  return result
end
