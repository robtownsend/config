vim.g.taskwiki_maplocalleader='\' 

vim.g:vimwiki_syntax = 'markdown'
vim.g:vimwiki_ext = '.md'
vim.g:vimwiki_links_space_char = '_'
vim.g:vimwiki_dir_link = 'index'
vim.g:vimwiki_global_ext = 0

let life = {}
let life.path = "~/.vimwiki/life/"
let life.name = 'Life'

let career = {}
let career.path = "~/.vimwiki/career/"
let career.name = 'Career'

let work = {}
let work.path = "~/.vimwiki/work/"
let work.name = 'Work'

let vol = {}
let vol.path = "~/.vimwiki/volunteering/"
let vol.name = 'Volunteering'

vim.g:vimwiki_list = { life, career, work, vol }

vim.cmd([[ call vimwiki#vars#init() ]])

