let g:pandoc#filetypes#handled = ["pandoc", "markdown"]                                               
let g:pandoc#filetypes#pandoc_markdown = 0

" workaround for bug in vim-pandoc. See github.com/vim-pandoc/vim-pandoc/issues/342
augroup SetCorrectFiletype
	autocmd!
  autocmd BufRead,BufNewFile *.md set filetype=markdown
augroup END

