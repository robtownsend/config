-- local function list_branches()
--   let git = 'git -C ' . getcwd()
--   let branches = systemlist(git .' branch | head -n10')
--   " let git = 'G'. git[1:]
--   return map(branches, '{"line": matchstr(v:val, "\\s\\zs.*"), "cmd": "'. git .' checkout ". matchstr(v:val, "^\\x\\+") }')
-- endfunction

-- let g:startify_lists = [
--   \ { 'type': function('s:list_branches'), 'header': ['Branches'] },
--   \ { 'type': 'sessions', 'header': ['Sessions'] },
--   \ { 'type': 'dir', 'header': [getcwd()] }
--   \ ]

-- vim.opt.startify_change_to_dir = 0
-- vim.opt.startify_session_autoload = 1
-- vim.opt.startify_session_persistence = 1
-- vim.opt.startify_session_sort = 1 " Sort by mru instead of alphabetically

-- " Generate session named after current git branch
-- function getUniqueSessionName() do
--   let path = fnamemodify(getcwd(), ':~:t')
--   let path = empty(path) ? 'no-project' : path
--   let branch = gitbranch#name()
--   let branch = empty(branch) ? '' : '-' . branch
--   return substitute(path . branch, '/', '-', 'g')
-- end

-- -- Auto-load session for current branch
-- vim.cmd('augroup BranchSessions')
-- vim.cmd('autocmd User CheckoutBranchSelection silent execute 'SLoad '  . GetUniqueSessionName()'
-- -- autocmd VimLeavePre *             silent execute 'SSave! ' . GetUniqueSessionName()
-- vim.cmd('augroup END')


-- -- " let g:startify_custom_header = startify#center([
-- -- " \ '       __.,,------.._                                                        ',
-- -- " \ '     ,'"   _      _   "`.                                                    ',
-- -- " \ '    /.__, ._  -=- _"`    Y                                                   ',
-- -- " \ '   (.____.-.`      ""`   j                                                   ',
-- -- " \ '    VvvvvvV`.Y,.    _.,-'       ,     ,     ,                                ',
-- -- " \ '        Y    ||,   '"\         ,/    ,/    ./                                ',
-- -- " \ '        |   ,'  ,     `-..,'_,'/___,'/   ,'/   ,                             ',
-- -- " \ '   ..  ,;,,',-'"\,'  ,  .     '     ' ""' '--,/    .. ..                     ',
-- -- " \ ' ,'. `.`---'     `, /  , Y -=-    ,'   ,   ,. .`-..||_|| ..                  ',
-- -- " \ 'ff\\`. `._        /f ,'j j , ,' ,   , f ,  \=\ Y   || ||`||_..               ',
-- -- " \ 'l` \` `.`."`-..,-' j  /./ /, , / , / /l \   \=\l   || `' || ||...            ',
-- -- " \ ' `  `   `-._ `-.,-/ ,' /`"/-/-/-/-"'''"`.`.  `'.\--`'--..`'_`' || ,          ',
-- -- " \ '            "`-_,',  ,'  f    ,   /      `._    ``._     ,  `-.`'//         ,',
-- -- " \ '          ,-"'' _.,-'    l_,-'_,,'          "`-._ . "`. /|     `.'\ ,       |',
-- -- " \ '        ,',.,-'"          \=) ,`-.         ,    `-'._`.V |       \ // .. . /j',
-- -- " \ '        |f\\               `._ )-."`.     /|         `.| |        `.`-||-\\/ ',
-- -- " \ '        l` \`                 "`._   "`--' j          j' j          `-`---'  ',
-- -- " \ '         `  `                     "`_,-','/       ,-'"  /                    ',
-- -- " \ '                                 ,'",__,-'       /,, ,-'                     ',
-- -- " \ '                                 Vvv'            VVv'                        ',
-- -- " \ ])
