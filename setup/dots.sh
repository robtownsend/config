#!/bin/bash
set -e

dots_dir="$HOME/dev/conf/dots"
source "$dots_dir/setup/helpers/symlink_conf_dirs.sh"
source "$dots_dir/setup/helpers/install_awesomewm_deps.sh"
source "$dots_dir/setup/helpers/load_secrets.sh"
source "$dots_dir/setup/helpers/setup_jobs.sh"
source "$dots_dir/setup/helpers/hyper_mod.sh"
source "$dots_dir/setup/helpers/clone_media.sh"

mkdir -p "$HOME/.config"

echo "Linking config dirs"
symlink_conf_dirs
echo "Done."

echo "Loading secrets"
load_secrets
echo "Done."

echo "Downloading dependencies for awesomewm"
install_awesomewm_deps
echo "Done."

echo "Enabling hyper modifier"
hyper_mod
echo "Done."

echo "Registering jobs"
setup_jobs
echo "Done."

echo "Cloning media"
clone_media
echo "Done."

echo "Done configuring dots"
