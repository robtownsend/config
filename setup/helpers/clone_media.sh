#!/usr/bin/env bash
set -e

clone_media() {
  local dest="$HOME/data/annex"
  local ssh_cmd="ssh -i $HOME/.ssh/taskd/taskd"
  local repo="ssh://taskd@taskd:/home/taskd/annex.git"

  if [[ -d "$dest" ]]; then
    return 0
  fi

  export GIT_SSH_COMMAND="$ssh_cmd"
  git clone "$repo" "$dest"

  cd "$dest"
  git config core.sshCommand "$ssh_cmd"

  git annex init
  git annex get --all

  unset GIT_SSH_COMMAND
}
