#!/usr/bin/env bash
set -e

X11_dir="/usr/share/X11/xkb"
capslock_file="$X11_dir/symbols/capslock"
evdev_file="$X11_dir/keycodes/evdev"

get_line_num() {
  local file="$1"
  local num="$2"

  awk "NR == $2" "$1"
}

line_num_contains() {
  local file="$1"
  local num="$2"
  local str="$3"
  local line

  line="$(get_line_num "$file" "$num")"
  grep -q "$str" < <(echo "$line")
}

hyper_mod() {
  if line_num_contains "$capslock_file" 69 "Mod4"; then
    sudo sed -i '69s/Mod4/Mod3/' "$capslock_file"
  fi

  # Comment HYPR assignment if not already
  pattern='^\s*<HYPR'
  if grep -q -E "$pattern" "$evdev_file" ; then
    sudo sed -i 's/^\s*<HYPR>/\/\/<HYPR>/' "$evdev_file"
  fi
}
