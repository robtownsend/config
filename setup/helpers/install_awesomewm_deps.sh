#!/usr/bin/env bash
set -e

install_awesomewm_deps() {
  declare -A deps=(
    ["awesome-workspace-grid"]="http://github.com/lmanul/awesome-workspace-grid"
    ["awesomewm-vim-tmux-navigator"]="http://github.com/intrntbrn/awesomewm-vim-tmux-navigator"
    ["bling"]="http://github.com/BlingCorp/bling"
  )
  awesome_dir="$HOME/.config/awesome"

  for d in "${!deps[@]}"; do
    if [[ ! -d "$awesome_dir/$d" ]]; then
      git clone "${deps[$d]}" "$awesome_dir/$d"
    fi
  done
}
