#!/usr/bin/env bash
set -e

load_secret() {
  local pass_path="$1"

  pass "$pass_path" | head -n 1
}

load_secrets() {
  local secrets_file="$HOME/.config/zsh/.secrets"
  local secret
  local pass_path

  if [[ ! -f "$secrets_file" ]]; then
    touch "$secrets_file"
  fi

  # env_var_key=pass path
  declare -A secrets=(
    ["PUSHOVER_API_KEY"]="pushover/api_key"
    ["PUSHOVER_USER_KEY"]="pushover/user_key"
    ["PUSHOVER_DEVICE_ZENFONE"]="pushover/devices/zenfone"
    ["PUSHOVER_DEVICE_MOTO"]="pushover/devices/moto"
  )

  for key in "${!secrets[@]}"; do
    if grep -q "$key" "$secrets_file"; then
      continue
    fi

    pass_path="${secrets[$key]}"
    secret=$(load_secret "$pass_path")

    echo "export $key=$secret" >> "$secrets_file"
  done
}
