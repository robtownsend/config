#!/usr/bin/env bash
set -e

setup_jobs() {
  local bin_dir="/home/rob/dev/conf/dots/bin"

  $bin_dir/register_jobs
  $bin_dir/register_consumers
}
