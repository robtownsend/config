#!/usr/bin/env bash
set -e

symlink_conf_dirs() {
  # src paths are relative to dots top-level
  declare -A dots=(
    ["arch/conky"]="$HOME/.config/conky"
    ["arch/dunst"]="$HOME/.config/dunst"
    ["arch/i3"]="$HOME/.config/i3"
    ["arch/i3/slate.rasi"]="$HOME/slate.rasi"
    ["arch/awesome"]="$HOME/.config/awesome"
    ["arch/keys"]="$HOME/.config/keys"
    ["arch/kitty"]="$HOME/.config/kitty"
    ["arch/picom"]="$HOME/.config/picom"
    ["arch/polybar"]="$HOME/.config/polybar"
    ["arch/sxhkd"]="$HOME/.config/sxhkd"
    ["git/gitconfig"]="$HOME/.gitconfig"
    ["git/hooks/pass_auto_push.sh"]="$HOME/.password-store/.git/hooks/post-commit"
    ["arch/zsh"]="$HOME/.config/zsh"
    ["nvim"]="$HOME/.config/nvim"
    ["starship/starship.toml"]="$HOME/.config/starship.toml"
    ["taskwarrior"]="$HOME/.config/taskwarrior"
    ["tmux"]="$HOME/.config/tmux"
    ["zshenv"]="$HOME/.zshenv"
  )

  dots_dir="$HOME/dev/conf/dots"
  for src in "${!dots[@]}"; do
    orig="$dots_dir/$src"
    dest="${dots[$src]}"

    if [[ -e "$dest" || -L "$dest"  ]]; then
      echo "Running rm -rf $dest"
      rm -rf "$dest"
    fi

    if [[ ! -d $(dirname "$dest") ]]; then
      continue
    fi

    echo "Running ln -s $orig $dest"
    ln -s "$orig" "$dest"
  done
}
