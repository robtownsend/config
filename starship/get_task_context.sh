#!/usr/bin/env bash

source "/home/rob/dev/conf/dots/lib/utils/join_arr.sh"
parser="my @matches = /Context \'(\w+)\'/; print @matches[0];"

context=$(task context show | perl -ne "$parser")
readarray -d ' ' active_tasks < <(task +ACTIVE ids)


echo "($context) $( join_arr ${active_tasks[@]} )"
