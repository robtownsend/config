package main

import (
	"encoding/json"
	"fmt"
	"os/exec"
	"slices"
	"strconv"
	"strings"
)

type Tree struct {
	Name     string  `json:"name"`
	Children []*Tree `json:"children"`
}

type Task struct {
	ID   int    `json:"id"`
	Desc string `json:"desc"`
}

// Helpers

// pipe str to shell program
func pipeStr(str string, cmd *exec.Cmd) *exec.Cmd {
	pipe, _ := cmd.StdinPipe()
	defer pipe.Close()

	pipe.Write([]byte(str))

	return cmd
}

func pipeSeq(cmds []*exec.Cmd) string {
	if len(cmds) == 0 {
		return ""
	}

	current := cmds[0]
	var next *exec.Cmd
	var output []byte

	for _, c := range cmds[1:] {
		next = c

		pipe, _ := current.StdoutPipe()
		next.Stdin = pipe
		defer pipe.Close()

		current.Start()
		current = next
	}

	output, _ = current.Output()

	return strings.TrimSpace(string(output))
}

func findIndex(haystack []string, needle string) int {
	for i, str := range haystack {
		if str == needle {
			return i
		}
	}

	return -1
}

func getIndent(amt int) string {
	whitespace := ""

	for range amt {
		whitespace = fmt.Sprintf("%s ", whitespace)
	}

	return whitespace
}

func indent(amt int) {
	fmt.Print(getIndent(amt))
}

func getTasksForProj(proj string, filter string) []Task {
	pairs := pipeSeq([]*exec.Cmd{
		exec.Command("task", fmt.Sprintf("proj.is:%s", proj), filter, "status:pending", "export"),
		exec.Command("jq", "-rc", ".[] | { ID: .id, Desc: .description }"),
	})

	arr := make([]Task, 0)
	if pairs == "" {
		return arr
	}

	for _, pair := range strings.Split(pairs, "\n") {
		var t Task
		json.Unmarshal([]byte(pair), &t)

		arr = append(arr, t)
	}

	return arr
}

func getProjects() []string {
	tasks := pipeSeq([]*exec.Cmd{
		exec.Command("task", "flat-projects"),
		exec.Command("uniq"),
		exec.Command("tail", "+4"), // Skips header text
	})

	return strings.Split(tasks, "\n")
}

func makeTree(root *Tree, projs []string) {
	for _, proj := range projs {
		levels := strings.Split(proj, ".")

		makeTreeHelper(root, levels)
	}
}

func makeTreeHelper(node *Tree, levels []string) {
	if len(levels) == 0 {
		return
	}

	if node.Name == levels[0] {
		makeTreeHelper(node, levels[1:])
		return
	}

	childrenNames := make([]string, 0)
	for _, n := range node.Children {
		childrenNames = append(childrenNames, n.Name)
	}

	if slices.Contains(childrenNames, levels[0]) {
		idx := findIndex(childrenNames, levels[0])
		makeTreeHelper(node.Children[idx], levels[1:])

		return
	}

	subTree := Tree{
		Name:     levels[0],
		Children: make([]*Tree, 0),
	}
	node.Children = append(node.Children, &subTree)

	makeTreeHelper(&subTree, levels[1:])
}

// dfs on each node
func walkTree(root Tree) {
	for _, node := range root.Children {
		walkTreeHelper(node, 0, "")
	}
}

func bold(str string) string {
	boldCmd := exec.Command("tput", "bold")
	regCmd := exec.Command("tput", "sgr0")
	bold, _ := boldCmd.Output()
	reg, _ := regCmd.Output()

	return fmt.Sprintf("%s%s%s", bold, str, reg)
}

func center(str string, totalWidth int) string {
	width := totalWidth - len(str)

	return getIndent(width/2) + str + getIndent(width/2)
}

func printProjHeading(proj string) {
	width := 159

	fmt.Println(center(bold(proj), width))
	fmt.Println()
}

func maxWidth(strs []string) int {
	maxLen := 0

	for _, s := range strs {
		if len(s) > maxLen {
			maxLen = len(s)
		}
	}

	return maxLen
}

func mapFn[T, U any](vals []T, fn func(T) U) []U {
	arr := make([]U, 0)

	for _, v := range vals {
		arr = append(arr, fn(v))
	}

	return arr
}

func findWordBound(text string, end int) int {
  start := end - 1

  for {
    if text[start:end] == " " {
      break
    } else {
      start -= 1
      end -= 1
    }
  }

  return end
}

func getDescLines(text string, width int) []string {
	padding := 3
	if len(text) + padding < width {
		return []string{text}
	}

	lines := make([]string, 0)
	start := 0
	end := findWordBound(text, width - padding)

	for {
		lines = append(lines, strings.TrimSpace(text[start:end]))
		if end == len(text) {
			break
		}

		start = end
		end += width - padding

    // readjust
		if end > len(text) {
			end = len(text)
		} else {
      end = findWordBound(text, end)
    }
	}

	return lines
}

func formatTaskColumn(tasks []Task, width int /*, padding int */) []string {
	colWidth := width / 3
	innerPadding := 2

	lines := make([]string, 0)

	maxIdColWidth := maxWidth(mapFn(tasks, func(t Task) string {
		return strconv.Itoa(t.ID)
	}))
	descColWidth := colWidth - maxIdColWidth - innerPadding

	for _, t := range tasks {
		descLines := getDescLines(t.Desc, descColWidth)

		newLine := fmt.Sprintf("%-*d", maxIdColWidth, t.ID) + getIndent(innerPadding) + fmt.Sprintf("%s", descLines[0])
		lines = append(lines, newLine)

		for _, l := range descLines[1:] {
			newLine = getIndent(maxIdColWidth+innerPadding) + fmt.Sprintf("%-*s", descColWidth, l)
			lines = append(lines, newLine)
		}
	}

	return lines
}

func printTasksInColumns(pending []Task, inProgress []Task, upNext []Task) {
	width := 159
	colWidth := width / 3

	pendingColLines := formatTaskColumn(pending, width)
	inProgressColLines := formatTaskColumn(inProgress, width)
	upNextColLines := formatTaskColumn(upNext, width)

	maxLines := max(len(pendingColLines), len(inProgressColLines), len(upNextColLines))

	for i := range maxLines {
		if i < len(pendingColLines) {
			fmt.Printf("%-*s", colWidth, pendingColLines[i])
		} else {
			fmt.Print(getIndent(colWidth))
		}

		if i < len(inProgressColLines) {
			fmt.Printf("%-*s", colWidth, inProgressColLines[i])
		} else {
			fmt.Print(getIndent(colWidth))
		}

		if i < len(upNextColLines) {
			fmt.Printf("%-*s", colWidth, upNextColLines[i])
		}

		fmt.Print("\n")
	}
}

func walkTreeHelper(node *Tree, indentAmount int, parentPath string) {
	proj := fmt.Sprintf("%s%s", parentPath, node.Name)

	pendingTasks := getTasksForProj(proj, "-in_progress -next")
	inProgressTasks := getTasksForProj(proj, "+in_progress")
	upNextTasks := getTasksForProj(proj, "+next")

	if len(pendingTasks) > 0 || len(inProgressTasks) > 0 || len(upNextTasks) > 0 {
		printProjHeading(fmt.Sprintf("%s%s", parentPath, node.Name))

		printTasksInColumns(pendingTasks, inProgressTasks, upNextTasks)
		fmt.Println()
	}

	for _, ch := range node.Children {
		walkTreeHelper(ch, indentAmount+2, proj+".")
	}
}

func main() {
	root := Tree{
		Name:     "root",
		Children: make([]*Tree, 0),
	}

	projs := getProjects()
	makeTree(&root, projs)
	walkTree(root)
}
