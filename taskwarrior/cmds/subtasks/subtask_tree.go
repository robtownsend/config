package main

import (
	"fmt"
	"os/exec"
	"strings"
)

type TaskDeps struct {
  parent string
  deps []TaskDeps
}

// Ultimately want to print a tree-like view of tasks, with all its direct
// subtasks indented beneath it. For any children, if they belong to a 
// different project than the parent, we'll print that alongside the ID / desc

/*
Implementation-wise, we may need a couple passes over the task list, though
I think we'll be able to get by in just one. Because we have the subtasks
Tasks should only be printed once, and we need to subordinate 
*/

/*
So basically we'll end up with a list of linked lists. We can group the roots by project as well
*/

func pipeSeq(cmds []*exec.Cmd) string {
  if len(cmds) == 0 {
    return ""
  }

  current := cmds[0]
  var next *exec.Cmd
  var output []byte

  for _, c := range cmds[1:] {
    next = c

    pipe, _ := current.StdoutPipe()
    next.Stdin = pipe
    defer pipe.Close()

    current.Start()
    current = next
  }

  output, _ = current.Output()

  return strings.TrimSpace(string(output))
}

func populateProjectMap(ids []string) map[string][]string {
  rootProjs := make(map[string][]string)

  for _, id := range ids {
    proj := pipeSeq([]*exec.Cmd{
      exec.Command("task", id, "_project"),
    })

    if proj == "" {
      continue
    }

    rootProjs[proj] = append(rootProjs[proj], id)
    // fmt.Println("Proj list is ", len(rootProjs[proj]), " elems")
    // fmt.Printf("%+v\n", rootProjs[proj])
  }

  return rootProjs
}

func getRootIds() []string {
  rootIds := pipeSeq([]*exec.Cmd{
    exec.Command("task", "parent_task.none:", "status:pending", "_ids"),
  })

  return strings.Split(rootIds, "\n")
}

func indent(n int) {
  for range n {
    fmt.Printf(" ")
  }
}

func makeDepsList(id string) TaskDeps {
  var noop TaskDeps
  if (id == "") {
    return noop
  }

  // fmt.Println("Recursing into makeDepsList w/ id ", id)
  depIds := pipeSeq([]*exec.Cmd{
    exec.Command("subtasks", id),
    exec.Command("tail", "-n+2"),
    exec.Command("perl", "-ne", "use feature qw(say); m/(\\d+)\\s-\\s.*/; say $1"),
  })

  deps := make([]TaskDeps, 0)
  for _, dep := range strings.Split(depIds, "\n") {
    deps = append(deps, makeDepsList(dep))
  }

  return TaskDeps{
    parent: id,
    deps: deps,
  }
}

func printDepsList(depsList TaskDeps) {
  printDepsListHelper(depsList, 0)
}

func printTask(id string) {
  if id == "" {
    return
  }

  desc := pipeSeq([]*exec.Cmd{
    exec.Command("task", id, "export"),
    exec.Command("jq", "-r", ".[] | .description"),
  })

  fmt.Printf("%s - %s\n", id, desc)
}

func printDepsListHelper(depsList TaskDeps, ndnt int) {
  id := depsList.parent
  indent(ndnt)
  printTask(id)

  for _, dep := range depsList.deps {
    printDepsListHelper(dep, ndnt + 2)
    fmt.Println()
  }
}

func main() {
  ids := getRootIds()
  projectsMap := populateProjectMap(ids)

  // for proj, ids := range projectsMap {
  for _, ids := range projectsMap {
    for _, id := range ids {
      // fmt.Println("Making dep list for ", id)
      depsList := makeDepsList(id)
      printDepsList(depsList)
    }

    fmt.Println()
  }
}
