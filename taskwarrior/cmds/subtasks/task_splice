#!/usr/bin/env bash
set -x

source "/home/rob/dev/conf/dots/lib/utils/join_arr.sh"
source "/home/rob/dev/conf/dots/lib/utils/in_array.sh"
source "/home/rob/dev/conf/dots/lib/utils/filter.sh"
source "/home/rob/dev/conf/dots/lib/utils/print_to_stderr.sh"

get_subtask_uuids() {
  local task="$1"

  readarray -t subtask_ids < <(\
    subtasks "$task" \
      | tail -n+2 \
      | perl -ne 'm/(\d+)\s-\s.*/; print $1 . "\n"'\
  )

  for id in "${subtask_ids[@]}"; do
    task "$id" uuids
  done
}

get_existing_deps() {
  local task="$1"
  local arr_name="$2"

  readarray -t "$arr_name" < <(subtasks "$task" \
    | tail -n+2 \
    | perl -ne 'm/(\d+)\s-\s.*/; print $1 . "\n"' \
  )
}

# prepend_all() {
#   return 0
# }

# Pretty standard doubly linked list work. Given a parent task:
# 1) . Save its original dependency
# 2) . Mark its dependency to be the given taskpointers
# 3) . Set the parent pointer of the given task to the parent
# 4) Mark the new task's dependency as the parent's original dep
# 5) Update the parent pointer of the parent task's original dep


# Additional ideas:
# - If the parent is the string "root", all
# existing tasks will become subtasks of the given list of commands, unless 
# They already had one.
#
# - If the parent is the string matching a project name, then all existing tasks
# will become subtasks of the given task, unless an existing parent is found

# Future expansions:
# Further, anything at the end of the list (ie leaves of the tree), should
# be tagged automatically with next (can be achieved w/ hook)

# Also may want a separate command built on top of this to mv a task in the list.
# This helper is essentially built for newly created tasks, but the ability to
# move existing tasks may also be needed 

if [[ -z "$1" || -z "$2" ]]; then
  print_to_stderr "task_splice: Invalid args"
  exit 1
fi

parent="$1"
given_task="$2"

# Catch case when root is passed in as parent
# if [[ $parent =~ [[:alpha:]]+ ]]; then
#   # if [[ $parent == "root" ]]; then
#   #   prepend_all $@
#   #   return $?
#   # fi
#
#   echo "Matched re"
#
#   # else, look for a match against a project / subproject (ie writing or writing.prophecy)
#   return 0
# fi

# todo Add confirmation step + no-confirm flag

set_parent_pointer() {
  local task="$1"
  local parent="$2"

  task "$task" modify parent_task:"$parent"
}

update_parent_pointers() {
  local new_parent="$1"
  shift
  local children=( "$@" )

  for child in "${children[@]}"; do
    if [[ "$child" == "" ]]; then
      continue
    fi

    set_parent_pointer "$child" "$new_parent"
  done
}

assign_subtasks() {
  local task="$1"
  shift
  local subtasks=( "$@" )



  task "$task" modify depends:"$(join_arr "${subtasks[@]}")"
}

declare -a original_parent_deps
get_existing_deps "$parent" "original_parent_deps"

transfer_subtasks() {
  local new_parent="$1"
  shift
  local subtasks=( "$@" )

  if [[ "${#subtasks[@]}" -eq 0 ]]; then
    return 0
  fi

  # Possible todo. Should any existing deps for given tasks be union'd with the
  # parent deps? They're currently clobbered
  task "$given_task" modify depends:
  assign_subtasks "$given_task" "${subtasks[@]}"
  update_parent_pointers "$given_task" "${subtasks[@]}"
}

# Only transfer parent deps if any exist
if [[ "${#original_parent_deps[@]}" -gt 0 ]]; then
  if in_array "$given_task" "${original_parent_deps[@]}"; then
    filter_val filtered_parent_deps "$given_task" "${original_parent_deps[@]}"
  fi

  # shellcheck disable=SC2154
  if [[ ! ("${#filtered_parent_deps[@]}" -eq 1 && "${filtered_parent_deps[0]}" == "") ]]; then
    transfer_subtasks "$given_task" "${filtered_parent_deps[@]}"
  fi
fi

set_parent_pointer "$given_task" "$parent"

# todo add checks for actions thatll cause errors
