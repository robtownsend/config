package main

import (
	"os"
	"os/exec"
	"slices"
	"strings"
)

func pipeSeq(cmds []*exec.Cmd) string {
	if len(cmds) == 0 {
		return ""
	}

	current := cmds[0]
	var next *exec.Cmd
	var output []byte

	for _, c := range cmds[1:] {
		next = c

		pipe, _ := current.StdoutPipe()
		next.Stdin = pipe
		defer pipe.Close()

		current.Start()
		current = next
	}

	output, _ = current.Output()

	return strings.TrimSpace(string(output))
}

func filter_val(list []string, elem string) []string {
  arr := make([]string, 0)

  for _, v := range list {
    if elem == v {
      continue
    }

    arr = append(arr, elem)
  }

  return arr
}

func getSubtasks(task string) []string {
  str := pipeSeq([]*exec.Cmd{
    exec.Command("subtasks", task),
    exec.Command("tail", "-n+2"),
    exec.Command("awk", "{ match($0, /([[:digit:]]+)[[:space:]]*-.*/, arr); print arr[1]}" ),
  })

  return strings.Split(str, "\n")
}

func subtaskHelper(task string, subtasks []string, action string) {
  for _, st := range subtasks {
    var cmd *exec.Cmd

    if action == "remove" {
      cmd = exec.Command("task", task, "modify", "depends:-" + st)
    } else {
      cmd = exec.Command("task", task, "modify", "depends:" + st)
    }

    cmd.Output()
  }
}

func appendSubtasks(task string, subtasks []string) {
  subtaskHelper(task, subtasks, "append")
}

func removeSubtasks(task string, subtasks []string) {
  subtaskHelper(task, subtasks, "remove")
}

func setParentPointer(parent string, child string) {
  cmd := exec.Command("task", child, "modify", "parent_task:" + parent)
  cmd.Output()
}

func updateParentPointers(parent string, tasks []string) {
  for _, t := range tasks {
    setParentPointer(parent, t)
  }
}

func main() {
  parent := os.Args[1]
  given_task := os.Args[2]

  parent_subtasks := getSubtasks(parent)
  if len(parent_subtasks) > 0 {
    if slices.Contains(parent_subtasks, given_task) {
      parent_subtasks = filter_val(parent_subtasks, given_task)
    } else {
      appendSubtasks(parent, []string{given_task})
    }

    appendSubtasks(given_task, parent_subtasks)
    removeSubtasks(parent, parent_subtasks)
    updateParentPointers(given_task, parent_subtasks)
  }

  setParentPointer(parent, given_task)
}
