#!/usr/bin/env bash
set -e

is_task_id() {
  if [[ "$1" =~ ^[[:digit:]]+$ ]]; then
    echo "${BASH_REMATCH[1]}"
  else
    return 1
  fi
}
