package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os/exec"
	"strings"
	"os"
)

func get_task_subcommand() string {
  prefix := "command:"
  for _, arg := range os.Args[1:] {
    if (! strings.HasPrefix(arg, prefix)) {
      continue
    }

    startIndex := len(prefix)
    return arg[startIndex:]
  }

  return ""
}

func get_modified_task() string {
  stdin, err := io.ReadAll(os.Stdin)
  if (err != nil) {
    panic(err)
  }

  // oldtask export is on the first line, new task (ie after the 
  // modification) is input on line two
  return strings.Split(string(stdin), "\n")[1]
}

func notify(str string) {
  cmd := exec.Command("notify-send", str) 
  cmd.Run()
}

func add_in_progress_tag(tags []string) []string {
  newList := make([]string, len(tags))
  copy(newList, tags)

  tag := "in_progress"
  for _, s := range(tags) {
    if (s == tag) {
      return newList
    }
  }

  return append(newList, tag)
}

func filter(strs []string, str string) []string {
  newList := make([]string, 0)

  for _, s := range(strs) {
    if (s == str) {
      continue
    }

    newList = append(newList, s)
  }

  return newList
}

func remove_in_progress_tag(tags []string) []string {
  return filter(tags, "in_progress")
}

func get_tags_array(task map[string]any) []string {
  tags := task["tags"]
  newArr := make([]string, 0)

  if ( tags == nil ) {
    return newArr
  }

  for _, t := range tags.([]interface{}) {
    newArr = append(newArr, fmt.Sprint(t))
  }

  return newArr
}

// In order for the hook to resolve, the json of the modified task
// must be printed to stdout
func main() {
  var task map[string]any

  subCommand := get_task_subcommand()
  jsonStr := get_modified_task()

  if ! (subCommand == "start" || subCommand == "done") {
    fmt.Print(jsonStr)
    return
  }

  err := json.Unmarshal([]byte(jsonStr), &task)
	if err != nil {
    panic(err)
  }

  tags := get_tags_array(task)
  if (subCommand == "start") {
    tags = add_in_progress_tag(tags)
  } else if (subCommand == "done") {
    tags = remove_in_progress_tag(tags)
  }
  tags = filter(tags, "")

  task["tags"] = tags
  moddedJson, err := json.Marshal(task)
  if (err != nil) {
    notify("Failed parsing encoding json str")
    notify(err.Error())
    panic(err)
  }

  fmt.Printf("%s", moddedJson)
}
