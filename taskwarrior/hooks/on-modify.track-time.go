package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
  "os/exec"
	"strings"
)

func get_task_subcommand() string {
  prefix := "command:"
  for _, arg := range os.Args[1:] {
    if (! strings.HasPrefix(arg, prefix)) {
      continue
    }

    startIndex := len(prefix)
    return arg[startIndex:]
  }

  return ""
}

func get_task() string {
  stdin, err := io.ReadAll(os.Stdin)
  if (err != nil) {
    panic(err)
  }

  return strings.Split(string(stdin), "\n")[1]
}

func toggleTracking(subCmd string, uuid string) {
  cmd := exec.Command("timew", subCmd, uuid, ":yes")
  cmd.Run()
}

func main() {
  var task map[string]any
  modified_task := get_task()

  fmt.Print(modified_task)

  subcmd := get_task_subcommand()
  if (subcmd != "start" && subcmd != "stop") {
    os.Exit(0)
  }

  err := json.Unmarshal([]byte(modified_task), &task)
  if err != nil {
    panic(err)
  }

  uuid := task["uuid"].(string)
  // id := task["id"].(string)
  toggleTracking(subcmd, uuid)
}
