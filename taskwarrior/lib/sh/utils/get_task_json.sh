#!/usr/bin/env bash

get_task_json() {
  local id="$1"

  task "$id" export | jq -r '.[0]'
}
